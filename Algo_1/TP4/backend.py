#!/usr/bin/env python
#encoding: utf-8

import csv , os
import random

#---------------------------Constantes-----------------------------------

# Estos identificadores se utilizan dentro del archivo de estilos
# Para indicar de quién es cada carta. Si se modifican, es necesario
# modificar la hoja de estilos.
ESTILOS = { 1: "amigo", 2: "enemigo" }

#-----------------------------Clases-------------------------------------

class Juego (object):
    """ Clase que modela el juego Ultimate Triad. 
        Contiene la logica del juego y mantiene el estado del mismo. 
    """
    
    def __init__(self, jugador1, jugador2):
        """ Crea un nuevo juego a partir de los jugadores 
            (instancias de la clase Jugador) recibidos. 
            El juego se construye pero no se inicia.
        """    
        self.estado=False
        self.turno= 1
        self.jugadores_estilos={1:jugador1,2:jugador2}
        self.tablero=Tablero()
        self.dicc_cartas_orientaciones={}
        self.tablero_cartas={}
        self.dicc_cartas_jugadores={}
        self.valores_cartas()

    def iniciar_juego(self):
        """ Inicia el juego. """
        self.estado=True
        
    def esta_activo(self):
        """ Devuelve True si el juego se encuentra iniciado. 
            False en caso contrario. """
        if self.iniciar_juego():
            return self.estado
        else:
            self.estado=False
            return self.estado

    
    def obtener_jugadores(self):
        """ Devuelve una tupla con los jugadores que forman parte de este juego. """
        return (self.jugadores_estilos[1],self.jugadores_estilos[2])
    
    def obtener_tablero(self):
        """ Devuelve el tablero del juego. """
        return self.tablero
        
    def turno_de(self):
        """ Devuelve 1 si es el turno del jugador1 
            o 2 de ser el turno del jugador2. """
        return self.turno 
   
        
    def jugar_carta(self, id_carta, posicion_tablero):
        """ El jugador actula juega una carta en la posicion dada.
            id_carta es la posicion de la carta en la mano del jugador.
            Al finalizar se avanza el turno, y en caso de terminar el 
            juego, se lo marca como inactivo.
        """
        Jugador=self.jugadores_estilos[self.turno]
    
        if self.obtener_ganador():
            self.estado=False
        else:
            carta_instanciada = Jugador.mano_duplicado[id_carta]
            nombre_carta=carta_instanciada.obtener_nombre()
            self.insertar_posicion_carta(posicion_tablero,nombre_carta,carta_instanciada)
            Jugador.sacar_carta_mano(id_carta)
            cartas_conquistadas,posiciones_ganadas=self.atacar(posicion_tablero,nombre_carta)
            self.posesion_cartas_jugadores(cartas_conquistadas)
            self.actualizar_tablero(cartas_conquistadas,posiciones_ganadas) 
           
            self.turno=3-self.turno
            return self.turno_de()

                
    def obtener_ganador(self):
        """ Devuelve el jugador que gano el juego, en caso
            de que no haya terminado, devuelve None. """
        if self.tablero.esta_completo():
            jugador_1=1
            jugador_2=2
            cant_cartas_jugador_1=0
            cant_cartas_jugador_2=0
            lista=self.dicc_cartas_jugadores.values()
            cant_cartas_jugador_1=lista.count(1)
            cant_cartas_jugador_2=lista.count(2)
            if cant_cartas_jugador_1>cant_cartas_jugador_2:
                return self.jugadores_estilos[jugador_1]
            else:
                return self.jugadores_estilos[jugador_2]
        else:
            return None



    def insertar_posicion_carta(self,posicion_tablero,nombre_carta,carta_instanciada):
        """Insera carta y posicion en el tablero y estrcuturas"""
        self.tablero_cartas[posicion_tablero]=nombre_carta
        self.dicc_cartas_jugadores[nombre_carta]=self.turno
        self.tablero.colocar_carta(carta_instanciada,posicion_tablero)

    def posesion_cartas_jugadores(self,cartas_apropiadas):
        """Relaciona carta conquistada con su jugador"""
        if len(cartas_apropiadas)>0:
            for i in cartas_apropiadas:
                    self.dicc_cartas_jugadores[i]=self.turno
        else:
                pass


    def orientacion_contraria(self,orientacion):
        """Se define que punto cardinal ataca a otro"""
        dicc_contraria={"Sur":"Norte","Norte":"Sur","Este":"Oeste","Oeste":"Este"}
        orientacion_contra=dicc_contraria[orientacion]

        return orientacion_contra

    def traer_poder(self,nombre_carta,orientacion_carta):
        """Trae los valores de las cartas a atacar"""
        return self.dicc_cartas_orientaciones[nombre_carta][orientacion_carta]

    def traer_mi_poder(self,nombre_carta,orientacion_carta_contraria):
        """Se llama a funciones que retornar los valores de la carta que ataca y a los puntos cardinales que ataca"""
        
        orientacion_carta = self.orientacion_contraria(orientacion_carta_contraria)
        return self.traer_poder(nombre_carta,orientacion_carta)


    def atacar(self,mi_posicion,mi_carta):
        """Ataca para ello llama a otras funciones que conforman el ataque"""
        posiciones,orientaciones = self.atacar_cartas(mi_posicion,mi_carta)
        posiciones_atacables,orientaciones_atacables = self.si_se_puede_atacar(posiciones,orientaciones)

        cartas_ganadas = []
        posiciones_ganadas=[]

        if len(posiciones_atacables) > 0 :

            for i in range(0,len(posiciones_atacables)):
                nombre_carta = self.tablero_cartas[posiciones_atacables[i]]
                orientacion_carta = orientaciones_atacables[i]

                poder_otro = self.traer_poder(nombre_carta,orientacion_carta)

                mi_poder = self.traer_mi_poder(mi_carta,orientacion_carta)

                if(poder_otro < mi_poder):
                    cartas_ganadas.append(nombre_carta)
                    posiciones_ganadas.append(posiciones_atacables[i])

        
        return cartas_ganadas,posiciones_ganadas

    def si_se_puede_atacar(self,posicion,orientacion):
        """ Verifica que la carta a atacar no sea propia y que si hay alguien a quien atacar"""

        posiciones_atacables = []
        orientaciones_atacables = []

        for i in range(0,len(posicion)):
            if(self.tablero_cartas.has_key(posicion[i])) and self.dicc_cartas_jugadores[self.tablero_cartas[posicion[i]]]!= self.turno:
                posiciones_atacables.append(posicion[i])
                orientaciones_atacables.append(orientacion[i])

        return posiciones_atacables,orientaciones_atacables


    def atacar_cartas(self,mi_posicion,nombre_carta):
        """Retorna a que punto cardinal puede atacar y la posiciones de las cartas a atacar"""
        dicc_posiciones_atacar={"A1":["A2","B1"],"A2":["A1","A3","B2"],"A3":["A2","B3"],"B1":["A1","B2","C1"],"B2":["A2","B1","B3","C2"],"B3":["A3","B2","C3"],"C1":["B1","C2"],"C2":["B2","C1","C3"],"C3":["B3","C2"]}
        dicc_orientaciones_atacar={"A1":["Oeste","Norte"],"A2":["Este","Oeste","Norte"],"A3":["Este","Norte"],"B1":["Sur","Oeste","Norte"],"B2":["Sur","Este","Oeste","Norte"],"B3":["Sur","Este","Norte"],"C1":["Sur","Oeste"],"C2":["Sur","Este","Oeste"],"C3":["Sur","Este"]}
        lista=dicc_posiciones_atacar[mi_posicion]
        orientacion=dicc_orientaciones_atacar[mi_posicion]

        return lista,orientacion

    def actualizar_tablero(self,cartas_conquistadas,posiciones_cartas):
        """Cambia el estilo de las cartas"""
        if len(cartas_conquistadas)>0:
            for i in range(len(cartas_conquistadas)):
                    carta=Carta(cartas_conquistadas[i],ESTILOS[self.turno])
                    self.tablero.colocar_carta(carta,posiciones_cartas[i])



    def valores_cartas(self):
        """Abre archivo_csv"""
        archivo=os.path.join('data','cartas.csv')
        archivo_=open(archivo)
        archivo_csv=csv.reader(archivo_,delimiter=";")
        for row in archivo_csv:
            if "A" in row[1]:
                row[1]=10
            if "A" in row[2]:
                row[2]=10
            if "A" in row[3]:
                row[3]=10
            if "A" in row[4]:
                row[4]=10
            self.dicc_cartas_orientaciones[row[0]] = {'Norte': int(row[1]), 'Sur': int(row[2]), 'Este': int(row[3]),'Oeste': int(row[4])}
   
        archivo_.close()


        
        
class Jugador (object):
    """ Modela un jugador en el juego Ultimate Triad """

   
    def __init__(self, nombre, id_jugador, nombre_mazo, cartas):
        """ Crea un nuevo jugador con los atributos recibidos
            id_jugador corresponde al identificador de jugador en el juego.
            cartas es una lista con los nombres de las cartas del mazo"""
        self.nombre=nombre
        self.id_jugador=id_jugador
        self.cartas=[]
        self.mazo_nombre=nombre_mazo
        self.cartas_instanciadas(cartas)
        self.mano=self._generar_mano(self.cartas)
        self.mano_duplicado=self.mano
        


    def _generar_mano(self, lista_cartas): 
        """ Genera una mano en base a la lista de cartas
            pasada"""
        mano=[]
        random.shuffle(lista_cartas)
        for i in range(5):
            mano.append(lista_cartas[i])
        return mano
            
    def obtener_nombre(self):
        """ Devuelve el nombre del jugador. """
        return self.nombre
        
    def obtener_nombre_mazo(self):
        """ Devuelve el nombre del mazo del jugador. """ 
        return self.mazo_nombre
        
    def obtener_mano(self):
        """ Devuelve una lista de python con las cartas de la mano. """
        return self.mano
        
    def sacar_carta_mano(self, posicion):
        """ Elimina una carta de la mano del jugador en la posicion
            dada, la cual se corresponde a una posicion de la lista
            que devuelve obtener_mano"""
        self.mano.pop(posicion)

    def cartas_instanciadas(self,cartas):
        """Crea instancias de cartas"""  
        for i in cartas:
             self.cartas.append(Carta(i.strip(),ESTILOS[self.id_jugador]))



    
class Carta(object):
    """ Modela una carta en el juego, con un nombre, que es constante, 
        y un estilo que puede ir cambiando a lo largo del juego.
    """
    def __init__(self, nombre, estilo):
        """ Crea una nueva carta con el nombre y el estilo recibidos. """
        self.nombre=nombre
        self.estilo=estilo
        
    def obtener_nombre(self):
        """ Devuelve el nombre de la carta. """
        return self.nombre
        
    def obtener_estilo(self):
        """ Devuelve el estilo de la carta. """


        return self.estilo



class Tablero(object):
    """ Modela un tablero de 3x3 para el juego Ultimate Triad """
    
    def __init__(self):
        """ Crea un tablero vacio"""
        self.dicc_posiciones={"A1":None,"A2":None,"A3":None,"B1":None,"B2":None,"B3":None,"C1":None,"C2":None,"C3":None}
        self.estado=True
                        
    def colocar_carta(self, carta, posicion):
        """ Coloca en el tablero la carta recibida en la posicion indicada.
            Las posiciones validas son:            
            'A1' 'A2' 'A3'
            'B1' 'B2' 'B3'
            'C1' 'C2' 'C3'
        """
      
        self.dicc_posiciones[posicion]=carta
        

        
    def obtener_carta(self, posicion):
        """ Devuelve la carta jugada en la posicion indicada 
        o None si no hay una carta jugada en esa posicion. """

        return self.dicc_posiciones[posicion]

        
        
    def esta_completo(self):
        """ Devuelve True si el tablero esta completo. False en caso contrario. """
        lista=self.dicc_posiciones.values()
        for i in lista:
            if i==None:
                return False
        return True



# ********************************************************************** #
# A partir de aca son clases ya implementadas que no tienen que modificar#
# ********************************************************************** #

import sqlite3

class Usuario(object):
    """ Modela un usuario en el sistema. Sus atributos son:

        * self.uid       - Identificador unico de usuario (entero)
        * self.nombre    - Nombre del usuario
        * self.passwd    - Clave del usuario
    """
    def __init__(self, uid, nombre, passwd):
        """ Crea un nuevo usuario, con el id, nombre y clave recibidos. """        
        self.uid = uid
        self.nombre = nombre
        self.passwd = passwd
        
    
class Backend(object):
    """ Modela la base principal de operaciones de la pagina.
        Permite crear y listar usuarios, asi como tambien jugadores.
    """
    # Constantes de rutas
    BASE = "."
    STATIC = os.path.join(BASE, "static")
    DATA = os.path.join(BASE, "data")
    CARTAS = os.path.join(DATA, "cartas.csv")
    USUARIOS = os.path.join(DATA, "usuarios.db")
    AVATARS = os.path.join(DATA, "avatars")
    DECKS = os.path.join(DATA, "decks")
 
    def __init__(self):
        """ Crea el backend """

        # Directorios necesarios
        self.crear_dir(self.DATA)
        self.crear_dir(self.AVATARS)
        self.crear_dir(self.DECKS)

        # Archivo con los datos de los usuarios
        self.initializar_db()

        # Datos relacionados a las partidas
        self.esperando = []
        self.partidas = {}
        self.partidas_usuarios = {}
        self.ultima_partida = 0

    # Manejo de archivos / directorios               
    def crear_dir(self, ruta):
        """ Si la ruta no existe, la crea; sino no hace nada. """
        if not os.path.exists(ruta):
            os.makedirs(ruta)

    def obtener_ruta_static(self, *args):
        """ Devuelve una ruta completa a un archivo dentro la carpeta de
            archivos estaticos"""
        return os.path.join(self.STATIC, *args)
        
    def obtener_ruta_avatar(self, *args):
        """ Devuelve una ruta completa a un archivo dentro la carpeta de
            archivos de avatars. """
        return os.path.join(self.AVATARS, *args)
        
    def obtener_ruta_cartas(self):
        """ Devuelve la ruta hacia el archivo de las cartas oficiales """
        return self.CARTAS
 
    # Base de datos de usuarios
    def initializar_db(self):
        """ Crea la base de datos en caso de que no exista. """

        # Si no existe el archivo, lo crea y crea la tabla
        if not os.path.exists(self.USUARIOS):
            self.cx = sqlite3.connect(self.USUARIOS)
            cu = self.cx.cursor()
            cu.execute("""
                CREATE TABLE Usuarios (
                    Id INTEGER PRIMARY KEY, 
                    Nombre TEXT,
                    Password TEXT
                ); """)
            self.cx.commit()

        # Si el archivo existe, simplemente inicia la conexion
        else:
            self.cx = sqlite3.connect(self.USUARIOS)
        
    def usuario(self, uid):
        """ Devuelve el usuario correspondiente al id dado, o None si no existe."""
        
        cu = self.cx.cursor()
        cu.execute(""" 
            SELECT Id, Nombre, Password
            FROM Usuarios
            WHERE Id = '%d';
            """ % uid)
        linea = cu.fetchone()

        if linea:
            return Usuario(linea[0], linea[1], linea[2])

        return None

    def nuevo_usuario(self, nombre, clave):
        """ Crea un nuevo usuario, devuelve su identificador, 
            o None si el nombre ya existe."""

        # Validacion: nombre y clave son obligatorios
        if not nombre or not clave:
            return None
        
        # Verificacion: el nombre no puede estar repetido
        cu = self.cx.cursor()
        cu.execute(""" 
            SELECT Id, Nombre, Password
            FROM Usuarios
            WHERE Nombre = '%s';
            """ % nombre)
        linea = cu.fetchone()
        # Si ya existe el usuario, devuelve None
        if linea:
            return None
       
        # Inserta el nuevo usuario
        cu.execute("""
            INSERT INTO Usuarios (Nombre, Password)
            VALUES ( '%s', '%s' )
            """ % (nombre, clave))
        uid = cu.lastrowid

        # Guarda los cambios
        self.cx.commit()

        # Crea un avatar vacio para el usuario
        self.avatar_vacio(nombre)

        return uid

    def usuarios(self):
        """ Devuelve la lista completa de usuarios. """
        resultado = []

        cu = self.cx.cursor()
        cu.execute(" SELECT Id, Nombre, Password FROM Usuarios ")
        linea = cu.fetchone()
        while linea:
            resultado.append(Usuario(linea[0], linea[1], linea[2]))
            linea = cu.fetchone()
        return resultado

    # Manejo de avatares
    def avatar_vacio(self, nombre):
        """ Lee un avatar por omision y lo guarda para este usuario. """
        try:
            contenido = open(os.path.join(self.STATIC, "blankUser.jpg") , "rb") 
            self.cambiar_avatar(nombre, contenido.read()) 
            contenido.close()
        except IOError:
            # En caso de error, guarda una foto vacia
            self.cambiar_avatar(nombre, None)

    def cambiar_avatar(self, nombre, contenido):
        """ Guarda la imagen en el directorio de avatars, en forma de un archivo .jpg """
        path_avatar = os.path.join(self.AVATARS, nombre + ".jpg")
        avatar = open(path_avatar,"wb")
        avatar.write(contenido)
        avatar.close()

    # Manejo de Mazos
    def guardar_mazo(self, nombre, contenido):
        """ Guarda el archivo de texto pasado en su correspondiente directorio
            en forma de un archivo .dck. contenido es un archivo abierto. """ 

        # Realiza las validaciones necesarias
        self.validar_mazo(contenido)

        # Una vez validado, guarda el mazo
        path_deck = os.path.join(self.DECKS, nombre + ".dck")
        deck = open(path_deck, "w")
        deck.write(contenido)
        deck.close()

    def validar_mazo(self, contenido):
        """ Verifica que las cartas ingresadas al mazo sean validas. """
        lineas = contenido.split("\n")
        oficiales = [ x[0] for x in self.obtener_cartas_oficiales() ]
        for carta in lineas[1:]:
            if carta.strip() and carta.strip() not in oficiales:
                raise ValueError(
                    "Mazo invalido. No se reconoce la carta: %s." % carta)

    def obtener_mazo(self, nombre):
        """ Devuelve la informacion del archivo .dck en forma de una lista de python
            El archivo es de la forma:
               Nombre Mazo
               carta
               carta
               ...
        """
        # Intenta abrir el archivo
        try:
            archivo = open(os.path.join(self.DECKS, nombre + ".dck"), "r")
        except IOError:
            return []

        # Lee el nombre y las cartas
        nombre = archivo.readline()
        cartas = archivo.readlines()
        archivo.close()

        # Devuelve el mazo
        return [nombre, cartas]

    def obtener_cartas_oficiales(self):
        """ Devuelve una lista con tuplas de la forma:
            ('nombreCarta',norte,sur,este,oeste).
            La lista tiene ordenadas las tuplas en base al nombre de carta"""
            
        resultado = []
        try:
            archivo_cartas = open( self.CARTAS ,'r')
        except IOError:
            raise IOError, 'Error al abrir el archivo de base de datos'
        lectorCSV = csv.reader(archivo_cartas, delimiter=';')
        for carta in lectorCSV:
            resultado.append((carta[0], carta[1], carta[2], carta[3], carta[4]))
        archivo_cartas.close()
        return resultado
         
    # Interaccion con la clase Juego 
    def usuario_esperando(self, nombre):
        """ Agrega un usuario a la lista de usuarios esperando para jugar. """
        if not nombre in self.esperando:
            self.esperando.append(nombre)

    def crear_jugador(self, nombre, identificador):
        """ Crea una nueva instancia de la clase jugador. """
        try:
            nombre_mazo, cartas = self.obtener_mazo(nombre)
        except (TypeError, ValueError):
            raise ValueError("No se pudo crear el mazo para %s" % nombre)

        return Jugador(nombre, identificador, nombre_mazo, cartas)

    def crear_partida(self, nombre1, nombre2):
        """ Crea una nueva partida para los usuarios indicados. """

        # Elimina el o los nombres si estaban esperando
        for nombre in nombre1, nombre2:
            if nombre in self.esperando:
                self.esperando.remove(nombre)

        # Crea los jugadores
        jugador1 = self.crear_jugador(nombre1, 1)
        jugador2 = self.crear_jugador(nombre2, 2)

        # Crea la partida
        partida = Juego(jugador1, jugador2)

        # Guarda la partida en los diccionarios
        self.ultima_partida += 1
        id_partida = self.ultima_partida
        self.partidas[id_partida] = partida
        for nombre in nombre1, nombre2:
            if nombre in self.partidas_usuarios:
                self.partidas_usuarios[nombre].append(id_partida)
            else:
                self.partidas_usuarios[nombre] = [id_partida]

        return id_partida

    def partida(self, id_partida):
        if id_partida in self.partidas:
            return self.partidas[id_partida]
        raise ValueError("La partida solicitada no existe")


