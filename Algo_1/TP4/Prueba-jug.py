import random,csv,os
ESTILOS = { 1: "amigo", 2: "enemigo" }

class Juego (object):
    """ Clase que modela el juego Ultimate Triad. 
        Contiene la logica del juego y mantiene el estado del mismo. 
    """
    
    def __init__(self, jugador1, jugador2):
        """ Crea un nuevo juego a partir de los jugadores 
            (instancias de la clase Jugador) recibidos. 
            El juego se construye pero no se inicia.
        """    
        self.estado=False
        self.turno= 1
        self.jugadores_estilos={1:jugador1,2:jugador2}
        self.tablero=Tablero()
        self.dicc_cartas_orientaciones={}
        self.tablero_cartas={}
        self.dicc_cartas_jugadores={}
        self.valores_cartas()

    def iniciar_juego(self):
        """ Inicia el juego. """
        self.estado=True
        
    def esta_activo(self):
        """ Devuelve True si el juego se encuentra iniciado. 
            False en caso contrario. """
        if self.iniciar_juego():
            return self.estado
        else:
            self.estado=False
            return self.estado

    
    def obtener_jugadores(self):
        """ Devuelve una tupla con los jugadores que forman parte de este juego. """
        return (self.jugadores_estilos[1],self.jugadores_estilos[2])
    
    def obtener_tablero(self):
        """ Devuelve el tablero del juego. """
        return self.tablero
        
    def turno_de(self):
        """ Devuelve 1 si es el turno del jugador1 
            o 2 de ser el turno del jugador2. """
        return self.turno 
   
        
    def jugar_carta(self, id_carta, posicion_tablero):
        """ El jugador actula juega una carta en la posicion dada.
            id_carta es la posicion de la carta en la mano del jugador.
            Al finalizar se avanza el turno, y en caso de terminar el 
            juego, se lo marca como inactivo.
        """
        Jugador=self.jugadores_estilos[self.turno]
    
        if self.obtener_ganador():
            self.estado=False
        else:
            carta_instanciada = Jugador.mano_duplicado[id_carta]
            nombre_carta=carta_instanciada.obtener_nombre()
            self.insertar_posicion_carta(posicion_tablero,nombre_carta,carta_instanciada)
            Jugador.sacar_carta_mano(id_carta)
            cartas_conquistadas,posiciones_ganadas=self.atacar(posicion_tablero,nombre_carta)
            self.posesion_cartas_jugadores(cartas_conquistadas)
            self.actualizar_tablero(cartas_conquistadas,posiciones_ganadas) 
           
            self.turno=3-self.turno
            return self.turno_de()

                
    def obtener_ganador(self):
        """ Devuelve el jugador que gano el juego, en caso
            de que no haya terminado, devuelve None. """
        if self.tablero.esta_completo():
            jugador_1=1
            jugador_2=2
            cant_cartas_jugador_1=0
            cant_cartas_jugador_2=0
            lista=self.dicc_cartas_jugadores.values()
            cant_cartas_jugador_1=lista.count(1)
            cant_cartas_jugador_2=lista.count(2)
            if cant_cartas_jugador_1>cant_cartas_jugador_2:
                return self.jugadores_estilos[jugador_1]
            else:
                return self.jugadores_estilos[jugador_2]
        else:
            return None



    def insertar_posicion_carta(self,posicion_tablero,nombre_carta,carta_instanciada):
        """Insera carta y posicion en el tablero y estrcuturas"""
        self.tablero_cartas[posicion_tablero]=nombre_carta
        self.dicc_cartas_jugadores[nombre_carta]=self.turno
        self.tablero.colocar_carta(carta_instanciada,posicion_tablero)

    def posesion_cartas_jugadores(self,cartas_apropiadas):
        """Relaciona carta conquistada con su jugador"""
        if len(cartas_apropiadas)>0:
            for i in cartas_apropiadas:
                    print "Las cartas apropiadas son",i
                    self.dicc_cartas_jugadores[i]=self.turno
        else:
                pass


    def orientacion_contraria(self,orientacion):
        """Se define que punto cardinal ataca a otro"""
        dicc_contraria={"Sur":"Norte","Norte":"Sur","Este":"Oeste","Oeste":"Este"}
        orientacion_contra=dicc_contraria[orientacion]

        return orientacion_contra

    def traer_poder(self,nombre_carta,orientacion_carta):
        """Trae los valores de las cartas a atacar"""
        return self.dicc_cartas_orientaciones[nombre_carta][orientacion_carta]

    def traer_mi_poder(self,nombre_carta,orientacion_carta_contraria):
        """Se llama a funciones que retornar los valores de la carta que ataca y a los puntos cardinales que ataca"""
        
        orientacion_carta = self.orientacion_contraria(orientacion_carta_contraria)
        return self.traer_poder(nombre_carta,orientacion_carta)


    def atacar(self,mi_posicion,mi_carta):
        """Ataca para ello llama a otras funciones que conforman el ataque"""
        posiciones,orientaciones = self.atacar_cartas(mi_posicion,mi_carta)
        posiciones_atacables,orientaciones_atacables = self.si_se_puede_atacar(posiciones,orientaciones)

        cartas_ganadas = []
        posiciones_ganadas=[]

        if len(posiciones_atacables) > 0 :

            for i in range(0,len(posiciones_atacables)):
                nombre_carta = self.tablero_cartas[posiciones_atacables[i]]
                orientacion_carta = orientaciones_atacables[i]

                poder_otro = self.traer_poder(nombre_carta,orientacion_carta)

                mi_poder = self.traer_mi_poder(mi_carta,orientacion_carta)

                if(poder_otro < mi_poder):
                    cartas_ganadas.append(nombre_carta)
                    posiciones_ganadas.append(posiciones_atacables[i])

        
        return cartas_ganadas,posiciones_ganadas

    def si_se_puede_atacar(self,posicion,orientacion):
        """ Verifica que la carta a atacar no sea propia y que si hay alguien a quien atacar"""

        posiciones_atacables = []
        orientaciones_atacables = []

        for i in range(0,len(posicion)):
            if(self.tablero_cartas.has_key(posicion[i])) and self.dicc_cartas_jugadores[self.tablero_cartas[posicion[i]]]!= self.turno:
                posiciones_atacables.append(posicion[i])
                orientaciones_atacables.append(orientacion[i])

        return posiciones_atacables,orientaciones_atacables


    def atacar_cartas(self,mi_posicion,nombre_carta):
        """Retorna a que punto cardinal puede atacar y la posiciones de las cartas a atacar"""
        dicc_posiciones_atacar={"A1":["A2","B1"],"A2":["A1","A3","B2"],"A3":["A2","B3"],"B1":["A1","B2","C1"],"B2":["A2","B1","B3","C2"],"B3":["A3","B2","C3"],"C1":["B1","C2"],"C2":["B2","C1","C3"],"C3":["B3","C2"]}
        dicc_orientaciones_atacar={"A1":["Oeste","Norte"],"A2":["Este","Oeste","Norte"],"A3":["Este","Norte"],"B1":["Sur","Oeste","Norte"],"B2":["Sur","Este","Oeste","Norte"],"B3":["Sur","Este","Norte"],"C1":["Sur","Oeste"],"C2":["Sur","Este","Oeste"],"C3":["Sur","Este"]}
        lista=dicc_posiciones_atacar[mi_posicion]
        orientacion=dicc_orientaciones_atacar[mi_posicion]

        return lista,orientacion

    def actualizar_tablero(self,cartas_conquistadas,posiciones_cartas):
        """Cambia el estilo de las cartas"""
        if len(cartas_conquistadas)>0:
            for i in range(len(cartas_conquistadas)):
                    carta=Carta(cartas_conquistadas[i],ESTILOS[self.turno])
                    self.tablero.colocar_carta(carta,posiciones_cartas[i])



    def valores_cartas(self):
        """Abre archivo_csv"""
        archivo=os.path.join('ultimatetriad-web', 'data','cartas.csv')
        archivo_=open(archivo)
        archivo_csv=csv.reader(archivo_,delimiter=";")
        for row in archivo_csv:
            if "A" in row[1]:
                row[1]=10
            if "A" in row[2]:
                row[2]=10
            if "A" in row[3]:
                row[3]=10
            if "A" in row[4]:
                row[4]=10
            self.dicc_cartas_orientaciones[row[0]] = {'Norte': int(row[1]), 'Sur': int(row[2]), 'Este': int(row[3]),'Oeste': int(row[4])}
   
        archivo_.close()

    
class Carta(object):
    """ Modela una carta en el juego, con un nombre, que es constante, 
        y un estilo que puede ir cambiando a lo largo del juego.
    """
    def __init__(self, nombre, estilo):
        """ Crea una nueva carta con el nombre y el estilo recibidos. """
        self.nombre=nombre
        self.estilo=estilo
        
    def obtener_nombre(self):
        """ Devuelve el nombre de la carta. """
        return self.nombre
        
    def obtener_estilo(self):
        """ Devuelve el estilo de la carta. """
        return self.estilo

    def __str__(self):
        return str(self.nombre)

class Jugador (object):
    """ Modela un jugador en el juego Ultimate Triad """

   
    def __init__(self, nombre, id_jugador, nombre_mazo, cartas):
        """ Crea un nuevo jugador con los atributos recibidos
            id_jugador corresponde al identificador de jugador en el juego.
            cartas es una lista con los nombres de las cartas del mazo"""
        self.nombre=nombre
        self.id_jugador=(id_jugador)
        self.cartas=[]
        
        for i in cartas:
             self.cartas.append(Carta(i.strip(),ESTILOS[id_jugador]))


        self.mazo_nombre=nombre_mazo
        self.mano=self._generar_mano(self.cartas)
        self.mano_duplicado=self.mano


    def _generar_mano(self, lista_cartas): 
        """ Genera una mano en base a la lista de cartas
            pasada"""
        mano=[]
        random.shuffle(lista_cartas)
        for i in range(5):
            mano.append(lista_cartas[i])
        return mano
            
    def obtener_nombre(self):
        """ Devuelve el nombre del jugador. """
        print "El nombre del jugador es :","\n", str(self.nombre)
        return self.nombre
        
    def obtener_nombre_mazo(self):
        """ Devuelve el nombre del mazo del jugador. """ 
        print "El nombre del mazo es:","\n",str(self.mazo_nombre)
        return self.mazo_nombre
        
    def obtener_mano(self):
        """ Devuelve una lista de python con las cartas de la mano. """
        print "mano de la carta","\n"
        for i in self.mano:
            print "El nombre de la carta es",str(i.obtener_nombre()),"El estilo de la carta es",str(i.obtener_estilo())
        return self.mano
        
    def sacar_carta_mano(self, posicion):
        """ Elimina una carta de la mano del jugador en la posicion
            dada, la cual se corresponde a una posicion de la lista
            que devuelve obtener_mano"""
        self.mano.pop(posicion)
        print "Con una carta menos el mazo queda","\n"
        for i in self.mano:
            print str(i.obtener_nombre())
    def __str__(self):
        return str(self.nombre)

class Tablero(object):
    """ Modela un tablero de 3x3 para el juego Ultimate Triad """
    
    def __init__(self):
        """ Crea un tablero vacio"""
        self.dicc_posiciones={"A1":None,"A2":None,"A3":None,"B1":None,"B2":None,"B3":None,"C1":None,"C2":None,"C3":None}
        self.estado=True
                        
    def colocar_carta(self, carta, posicion):
        """ Coloca en el tablero la carta recibida en la posicion indicada.
            Las posiciones validas son:            
            'A1' 'A2' 'A3'
            'B1' 'B2' 'B3'
            'C1' 'C2' 'C3'
        """
      
        self.dicc_posiciones[posicion]=carta
        print "La carta colocada es", self.dicc_posiciones[posicion].obtener_nombre()
        print "Su estilo es", self.dicc_posiciones[posicion].obtener_estilo()
        

        
    def obtener_carta(self, posicion):
        """ Devuelve la carta jugada en la posicion indicada 
        o None si no hay una carta jugada en esa posicion. """

        print "La carta colocada es", self.dicc_posiciones[posicion].obtener_nombre()
        print "Su estilo es", self.dicc_posiciones[posicion].obtener_estilo()
        return self.dicc_posiciones[posicion]


        
        
    def esta_completo(self):
        """ Devuelve True si el tablero esta completo. False en caso contrario. """
        lista=self.dicc_posiciones.values()
        for i in lista:
            if i==None:
                return False
        return True


J1=Jugador("pepe",1,"Dark Revelation",["Abadon","Bahamut","Cactuar","Diablos","Gilgamesh"])
J2=Jugador("juan",2,"Assasin Force",["Creeps","Doomtrain","Forbidden","Rinoa","Tiamat"])
J1.obtener_mano()
J2.obtener_mano()
J1.obtener_nombre_mazo()
J2.obtener_nombre_mazo()
J1.obtener_nombre()
J2.obtener_nombre()
J=Juego(J1,J2)
J.jugar_carta(1,"A1")
J.jugar_carta(2,"B1")



"""
Pruebas de la clase carta y jugador:
mano de la carta 

El nombre de la carta es Cactuar El estilo de la carta es amigo
El nombre de la carta es Bahamut El estilo de la carta es amigo
El nombre de la carta es Abadon El estilo de la carta es amigo
El nombre de la carta es Gilgamesh El estilo de la carta es amigo
El nombre de la carta es Diablos El estilo de la carta es amigo
mano de la carta 

El nombre de la carta es Creeps El estilo de la carta es enemigo
El nombre de la carta es Doomtrain El estilo de la carta es enemigo
El nombre de la carta es Rinoa El estilo de la carta es enemigo
El nombre de la carta es Forbidden El estilo de la carta es enemigo
El nombre de la carta es Tiamat El estilo de la carta es enemigo
El nombre del mazo es: 
Dark Revelation
El nombre del mazo es: 
Assasin Force
El nombre del jugador es : 
pepe
El nombre del jugador es : 
juan
La carta colocada es Bahamut
Su estilo es amigo
Con una carta menos el mazo queda 

Cactuar
Abadon
Gilgamesh
Diablos
La carta colocada es Rinoa
Su estilo es enemigo
Con una carta menos el mazo queda 

Creeps
Doomtrain
Forbidden
Tiamat
Las cartas apropiadas son Bahamut
La carta colocada es Bahamut
Su estilo es enemigo


"""

