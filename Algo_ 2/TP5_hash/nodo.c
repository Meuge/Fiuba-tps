#include "nodo.h"

typedef struct nodo
{
    struct nodo* siguiente;
    void* dato;
} t_nodo;

nodo_t* nodo_crear(void* data)
{
    nodo_t* nodo = malloc(sizeof(nodo_t));
    if (nodo == NULL) return NULL;
    nodo->siguiente=NULL;
    nodo->dato=data;
    return nodo;
}

void nodo_destruir(nodo_t* nodo,void destruir_dato(void *))
{
    if (destruir_dato!=NULL){
        destruir_dato(((diccionario_t*)nodo->dato)->dato);
    }else{
        /*free(((diccionario_t*)nodo->dato)->dato);*/
    }
    free(((diccionario_t*)nodo->dato)->clave);
    free((diccionario_t*)nodo->dato);
}

void nodo_set_siguiente(nodo_t* nodo, nodo_t* nodo_sig)
{
    nodo->siguiente=nodo_sig;
}

nodo_t* nodo_get_siguiente(nodo_t* nodo)
{
    return nodo->siguiente;
}

void* nodo_get_dato(nodo_t* nodo)
{
    return nodo->dato;
}

void nodo_set_dato(nodo_t* nodo,void* data)
{
    nodo->dato=data;
}
