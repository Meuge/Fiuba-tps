#include "hash.h"
#define HASH_LIMITE 0.5

hash_t *hash_crear(hash_destruir_dato_t destruir_dato)
{
    hash_t* hash=malloc(sizeof(hash_t));
    if (hash==NULL)return NULL;
    hash->tamanio=17; /*Numero primo */
    hash->tabla=calloc(hash->tamanio,sizeof(int));
    if(hash->tabla==NULL)
    {
        return NULL;
    }
    hash->cantidad=0;
    hash->destruir_dato=*destruir_dato;
    return hash;
}

bool hash_guardar(hash_t *hash, const char *clave, void *dato)
{
    diccionario_t* dicc;
    lista_iter_t* iter;
    size_t hash_valor=hash_funcion(hash, clave);
    if (hash->cantidad>=HASH_LIMITE*(hash->tamanio))
    {
        redimensionar_hash(hash);
        return hash_guardar(hash,clave,dato);
    }
    /*Redimensiona en caso de que supero el factor de carga como es un hash abierto
    considero un poco menor al cerrado */
    dicc=crear_diccionario(clave,dato);
    if (hash->tabla[hash_valor]!=NULL)
    {
        iter=lista_iter_crear((lista_t*)hash->tabla[hash_valor]);
        while (!lista_iter_al_final(iter))
        {
            if (!strcmp(((diccionario_t*)lista_iter_ver_actual(iter))->clave, clave))
            {
                lista_borrar(hash->tabla[hash_valor],iter,hash->destruir_dato);
                lista_insertar_primero(hash->tabla[hash_valor],dicc);
                lista_iter_destruir(iter);
                return true;
            }
            lista_iter_avanzar(iter);
        }
        lista_iter_destruir(iter);
        lista_insertar_primero((lista_t*)hash->tabla[hash_valor],dicc);
        hash->cantidad+=1;
        return true;
    }
    else
    {
        hash->tabla[hash_valor]=lista_crear();
        lista_insertar_primero(hash->tabla[hash_valor],dicc);
        hash->cantidad+=1;
        return true;
    }
}

void *hash_borrar(hash_t *hash, const char *clave)
{
    diccionario_t* data;
    size_t hash_valor;
    lista_iter_t* iter;
    hash_valor=hash_funcion(hash,clave);
    if (hash->tabla[hash_valor]!=NULL)
    {
        iter=lista_iter_crear(hash->tabla[hash_valor]);
        while (!lista_iter_al_final(iter))
        {
            if (!strcmp(((diccionario_t*)lista_iter_ver_actual(iter))->clave, clave))
            {
                data=lista_borrar(hash->tabla[hash_valor],iter,hash->destruir_dato);
                hash->cantidad-=1;
                lista_iter_destruir(iter);
                return data;
            }
            lista_iter_avanzar(iter);
        }
        lista_iter_destruir(iter);
        return NULL;
    }
    else
    {
        return NULL;
    }
}

void *hash_obtener(const hash_t *hash, const char *clave)
{
    diccionario_t *dicc;
    size_t hash_valor;
    lista_iter_t* iter;
    if (hash->tabla==NULL)return NULL;
    hash_valor=hash_funcion(hash,clave);
    if (hash->tabla[hash_valor]!=NULL)
    {
        iter=lista_iter_crear(hash->tabla[hash_valor]);
        while (!lista_iter_al_final(iter))
        {
            if (!strcmp(((diccionario_t*)lista_iter_ver_actual(iter))->clave, clave))
            {
                dicc=lista_iter_ver_actual(iter);
                lista_iter_destruir(iter);
                return dicc->dato;
            }
            lista_iter_avanzar(iter);
        }
        lista_iter_destruir(iter);
        return NULL;
    }
    else
    {
        return NULL;
    }
}

bool hash_pertenece(const hash_t *hash, const char *clave)
{
    size_t hash_valor;
    lista_iter_t* iter;
    if (hash->tabla==NULL)return false;
    hash_valor=hash_funcion(hash, clave);
    if (hash->tabla[hash_valor]!=NULL)
    {
        iter=lista_iter_crear(hash->tabla[hash_valor]);
        while (!lista_iter_al_final(iter))
        {
            if (!strcmp(((diccionario_t*)lista_iter_ver_actual(iter))->clave, clave))
            {
                lista_iter_destruir(iter);
                return true;
            }
            lista_iter_avanzar(iter);
        }
        lista_iter_destruir(iter);
        return false;
    }
    else
    {
        return false;
    }
}

size_t hash_cantidad(const hash_t *hash)
{
    return (hash->cantidad);
}

size_t hash_funcion(const hash_t*hash, const char *clave)
{
    size_t hash_valor=0;
    int j;
    size_t clave_hash;
    for (j=0; j<strlen(clave); j++)
    {
        hash_valor+=clave[j];
    }
    clave_hash=(hash_valor%hash->tamanio);
    return clave_hash;
}

void hash_destruir(hash_t *hash)
{
    int j;
    for(j=0; j<hash->tamanio; j++)
    {
        if (hash->tabla[j]!=NULL)
        {
            lista_destruir(hash->tabla[j],hash->destruir_dato);
        }
    }
    free(hash->tabla);
    free(hash);
}

hash_iter_t *hash_iter_crear(const hash_t *hash)
{
    int j=0;
    hash_iter_t* hash_iter=malloc(sizeof(hash_iter_t));
    if (hash_iter==NULL)
    {
        return NULL;
    }
    else
    {
        while(hash->tabla[j]==NULL && j<hash->tamanio-1)
        {
            j++;
        }
        if (hash->tabla[j]!=NULL)
        {
            hash_iter->posicion=j;
            hash_iter->hash=hash;
            hash_iter->list_iter=lista_iter_crear(hash->tabla[j]);
        }
        else
        {
            hash_iter->posicion=-1;
            hash_iter->hash=hash;
            hash_iter->list_iter=NULL;
        }
        return hash_iter;
    }
}

bool hash_iter_avanzar(hash_iter_t *iter)
{
    if (iter->posicion!=-1)
    {
        lista_iter_avanzar(iter->list_iter);
        if (lista_iter_al_final(iter->list_iter))
        {
            iter->posicion++;
            while (iter->posicion<iter->hash->tamanio && iter->hash->tabla[iter->posicion]==NULL)
            {
                iter->posicion++;
            }
            if (iter->hash->tamanio==iter->posicion)
            {
                iter->posicion=-1;
                return false;
            }
            else
            {
                lista_iter_destruir(iter->list_iter);
                iter->list_iter=lista_iter_crear(iter->hash->tabla[iter->posicion]);
                return true;
            }
        }
        else
        {
            return true;
        }
    }
    return false;
}

const char *hash_iter_ver_actual(const hash_iter_t *iter)
{
    if (iter->posicion==-1)
    {
        return NULL;
    }
    else
    {
        return ((diccionario_t*)lista_iter_ver_actual(iter->list_iter))->clave;
    }
}

bool hash_iter_al_final(const hash_iter_t *iter)
{
    if (iter->posicion==-1)
    {
        return true;
    }
    else
    {
        return false;
    }
}

void hash_iter_destruir(hash_iter_t* iter)
{
    lista_iter_destruir(iter->list_iter);
    free(iter);
}


void redimensionar_hash(hash_t *hash)
{
    hash_iter_t* iter_h;
    const char* clave_act;
    hash_t* hash_viejo=malloc(sizeof(hash_t));
    memcpy(hash_viejo,hash,sizeof(hash_t));
    hash->tamanio=hash->tamanio*2; /*Numero primo */
    hash->tabla=calloc(hash->tamanio*2,sizeof(int));
    hash->cantidad=0;
    iter_h=hash_iter_crear(hash_viejo);
    while (!hash_iter_al_final(iter_h)){
        clave_act=hash_iter_ver_actual(iter_h);
        hash_guardar(hash,clave_act,hash_obtener(hash_viejo,clave_act));
        hash_iter_avanzar(iter_h);
    }
    hash_iter_destruir(iter_h);
    hash_viejo->destruir_dato=NULL;
    hash_destruir(hash_viejo);
}

diccionario_t* crear_diccionario(const char*clave,void*dato)
{
    char* punt_clave;
    diccionario_t* dic;
    dic=malloc(sizeof(diccionario_t));
    if (dic==NULL) return NULL;
    punt_clave=malloc(20*sizeof(char));
    strcpy(punt_clave,clave);
    dic->dato=dato;
    dic->clave=punt_clave;
    return dic;
}

char *my_strdup(const char *s)
{
    char *p = malloc(strlen(s) + 1);
    if(p)
    {
        strcpy(p, s);
    }
    return p;
}














