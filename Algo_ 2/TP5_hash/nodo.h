#ifndef NODO_H
#define NODO_H

#include <stdlib.h>
#include <stdio.h>
#include <memory.h>
#include <string.h>
#include <stdbool.h>
#include <stddef.h>

/* ******************************************************************
 *                DEFINICION DE LOS TIPOS DE DATOS
 * *****************************************************************/

typedef struct diccionario{
    char* clave;
    void* dato;
}diccionario_t;

typedef struct nodo nodo_t;

/* ******************************************************************
 *                    PRIMITIVAS DE LA COL
 * *****************************************************************/

nodo_t* nodo_crear(void* data);

void nodo_destruir(nodo_t* nodo,void destruir_dato(void *));

void nodo_set_siguiente(nodo_t* nodo, nodo_t* nodo_sig);

nodo_t* nodo_get_siguiente(nodo_t* nodo);

void* nodo_get_dato(nodo_t* nodo);

void nodo_set_dato(nodo_t* nodo,void* data);

#endif /* NODO_H */
