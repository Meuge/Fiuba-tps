#ifndef HASH_H
#define HASH_H
#include "lista.h"


/* ******************************************************************
 *                DEFINICION DE LOS TIPOS DE DATOS
 * *****************************************************************/
typedef void (*hash_destruir_dato_t)(void *);
typedef struct hash{
	size_t cantidad;
	size_t tamanio;
	lista_t** tabla;
	hash_destruir_dato_t destruir_dato;
}hash_t;
typedef struct hash_iter{
    size_t posicion;
    const hash_t* hash;
    lista_iter_t* list_iter;
}hash_iter_t;


/* ******************************************************************
 *                    PRIMITIVAS DEL HASH
 * *****************************************************************/
 /*Crea el hash*/
hash_t *hash_crear(hash_destruir_dato_t destruir_dato);

/*Guarda el valor de la clave que se quiere  insertar, en tal caso retorna TRUE
Pre:El hash fue creado
Post: Se guarda el elemento con su respectiva clave en el hash, si ya existe se reemplaza el dato */
bool hash_guardar( hash_t *hash, const char *clave, void *dato);

/*Devuelve el valor a de la clave a borrar
Pre:El hash fue creado
Post: Verifica que la clave exista en tal caso retorna el valor, sino retorna NULL */
void *hash_borrar(hash_t *hash, const char *clave);

/*Devuelve el valor de la clave que se busca
Pre:El hash fue creado
Post:devuelve el valor del elemento buscado, sino devuelve NULL */
void *hash_obtener(const hash_t *hash, const char *clave);

/*Devuelve verdadero si la clave se encuentra en el hash, falso en caso negativo
Pre:El hash fue creado
Post: Devuelve un booleano por la clave a buscar*/
bool hash_pertenece(const hash_t *hash, const char *clave);

/*Devuelve la cantidad de elementos que hay en el hash
Pre: El hash fue creado
Post:Devuelve la cantidad de elementos del mismo */
size_t hash_cantidad( const hash_t *hash);

void hash_destruir(hash_t *hash);

/* ******************************************************************
 *                DEFINICION DE LOS FUNCIONES DEL ITERADOR
 * *****************************************************************/

hash_iter_t *hash_iter_crear(const hash_t *hash);

bool hash_iter_avanzar(hash_iter_t *iter);

const char *hash_iter_ver_actual(const hash_iter_t *iter);

bool hash_iter_al_final(const hash_iter_t *iter);

void hash_iter_destruir(hash_iter_t* iter);


/* ******************************************************************
 *                DEFINICION DE LOS FUNCIONES AUXILIARES
 * *****************************************************************/

size_t hash_funcion(const hash_t*hash, const char *clave);

void redimensionar_hash(hash_t *hash);

diccionario_t* crear_diccionario(const char*clave,void*dato);

char *my_strdup(const char *s);

#endif
