#ifndef NODO_H
#define NODO_H

#include "constantes.h"

/* ******************************************************************
 *                DEFINICION DE LOS TIPOS DE DATOS
 * *****************************************************************/

typedef struct nodo nodo_t;

typedef struct datos datos_t;



/* ******************************************************************
 *                    PRIMITIVAS DE LA COLA

 * *****************************************************************/

void destructor_pedidos(void* pedidos);

nodo_t* nodo_crear(const void* data);

void nodo_set_siguiente(nodo_t* nodo, nodo_t* nodo_sig);

datos_t* datos_crear(int cant,char* clie,char* zon,bool est);

datos_t* nodo_get_dato(nodo_t* nodo);

nodo_t* nodo_get_siguiente(nodo_t* nodo);

void datos_set_cliente(datos_t* data, char* clie);

char* datos_get_cliente(const datos_t* data);

void datos_set_cantidad(datos_t* data, int cant);

int datos_get_cantidad(datos_t* data);

int datos_get_cantidad(datos_t* data);

void datos_set_zona(datos_t* data, zonas zon);

zonas datos_get_zona(datos_t* data);

void datos_set_estado(datos_t* data, bool est);

bool datos_get_estado(datos_t* data);

datos_t* nodo_datos_crear(int cant,char* clie,zonas zon,bool est,int num);

void datos_set_numero(datos_t* data, int num);

int datos_get_numero(datos_t* data);

#endif /* NODO_H */
