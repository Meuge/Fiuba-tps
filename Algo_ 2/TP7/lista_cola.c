#include "lista_cola.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct lista_colas
{
    lista_t* pedidos;
    cola_t** cola_zonas;
    cola_t* enviados;
} t_lista_cola;


lista_cola_t* lista_cola_crear()
{
    int i;
    lista_cola_t* aux=malloc(sizeof(lista_cola_t));
    aux->cola_zonas=malloc(CANT_ZON*sizeof(void*));
    for(i=0; i<CANT_ZON; i++)
    {
        aux->cola_zonas[i]=cola_crear();
    }
    aux->pedidos=lista_crear();
    aux->enviados=cola_crear();
    return aux;
}

datos_t* buscar_cliente(lista_cola_t* lista_col, const datos_t* data,bool borrar)
{
    datos_t* retorno;
    lista_iter_t* iter;
    iter=lista_iter_crear(lista_col->pedidos);
    if(!lista_esta_vacia(lista_col->pedidos))
    {
        while (!lista_iter_al_final(iter))
        {
            if (!strcmp(datos_get_cliente(lista_iter_ver_actual(iter)),datos_get_cliente(data)))
            {
                if(borrar)
                {
                    retorno=lista_borrar(lista_col->pedidos,iter);
                    lista_iter_destruir(iter);
                    return retorno;
                }
                else
                {
                    retorno=lista_iter_ver_actual(iter);
                    lista_iter_destruir(iter);
                    return retorno;
                }
            }
            lista_iter_avanzar(iter);
        }
    }
    lista_iter_destruir(iter);
    return NULL;
}

bool ingresar_pedido(const datos_t *valor, lista_cola_t* lista_col)
{
    datos_t* aux;
    aux=buscar_cliente(lista_col,valor,false);
    if (aux==NULL)
    {
        lista_insertar_ultimo(lista_col->pedidos,valor);
        return true;
    }
    else
    {
        return false;
    }
}


bool modificar_pedido(lista_cola_t* lista_col,datos_t *valor,int cantidad)
{
    datos_t *data;
    if (cantidad==0)
    {
        data=buscar_cliente(lista_col,valor,true);
    }
    else
    {
        data=buscar_cliente(lista_col,valor,false);
    }
    if(!data)
    {
        return false;
    }
    else
    {
        if(cantidad!=0)
        {
            datos_set_cantidad(data,cantidad);
        }
        return true;
    }
}

bool preparar_pedido(lista_cola_t* lista_col)
{
    datos_t* dato_aux;
    int bucle;
    if (lista_get_tamanio(lista_col->pedidos)>1)
    {
        for(bucle=0; bucle<CANT_ESP; bucle++)
        {
            dato_aux=lista_borrar_primero(lista_col->pedidos);
            if (dato_aux!=NULL)
            {
                cola_encolar(lista_col->cola_zonas[datos_get_zona(dato_aux)],dato_aux);
            }
        }
        return true;
    }
    else
    {
        return false;
    }
}

bool enviar_pedido(lista_cola_t* lista_col)
{
    int menor,nro_zona,i,valor,cant;
    bool centinela;
    menor=9999;
    nro_zona=-1;
    for(i=0; i<CANT_ZON; i++)
    {
        if (lista_col->cola_zonas!=NULL && lista_col->cola_zonas[i]!=NULL)
        {
            if (!cola_esta_vacia(lista_col->cola_zonas[i]))
            {
                valor=datos_get_numero(cola_ver_primero(lista_col->cola_zonas[i]));
                if (valor<menor)
                {
                    menor=valor;
                    nro_zona=i;
                }
            }
        }
    }
    cant=0;
    centinela=true;
    while(centinela && cola_ver_primero(lista_col->cola_zonas[nro_zona])!=NULL)
    {
        centinela=false;
        cant=cant + datos_get_cantidad(cola_ver_primero(lista_col->cola_zonas[nro_zona]));
        if (cant<=5)
        {
            cola_encolar(lista_col->enviados,cola_desencolar(lista_col->cola_zonas[nro_zona]));
            centinela=true;
        }
    }
    return true;
}

bool cerrar_caja(lista_cola_t* lista_col)
{
    datos_t* dato_aux;
    lista_iter_t* iter;
    if (!lista_esta_vacia(lista_col->pedidos))
    {
        iter = lista_iter_crear(lista_col->pedidos);
        while(!lista_iter_al_final(iter))
        {
            dato_aux=lista_borrar_primero(lista_col->pedidos);
            if (dato_aux!=NULL)
            {
                cola_encolar(lista_col->cola_zonas[datos_get_zona(dato_aux)],dato_aux);
            }
            lista_iter_avanzar(iter);
        }
        lista_iter_destruir(iter);
        return true;
    }
    return false;
}

bool imprimir_registros(lista_cola_t* lista_col)
{
    lista_iter_t* iter;
    char* zona;
    iter=lista_iter_crear(lista_col->pedidos);
    if (lista_esta_vacia(lista_col->pedidos))
    {
        printf("Hasta el momento no se han ingresado pedidos\n");
    }
    else
    {
        printf ("Los pedidos registrados hasta el momento son los siguientes: \n");
        while (!lista_iter_al_final(iter))
        {
            printf("Pedido numero :%d \n",datos_get_numero(lista_iter_ver_actual(iter)));
            printf("Nombre del cliente:%s \n",datos_get_cliente(lista_iter_ver_actual(iter)));
            printf("Cantidad de pizzas pedidas: %d \n",datos_get_cantidad(lista_iter_ver_actual(iter)));
            switch(datos_get_zona(lista_iter_ver_actual(iter)))
            {
            case Z1:
            {
                zona="Z1";
                break;
            }
            case Z2:
            {
                zona="Z2";
                break;
            }
            case Z3:
            {
                zona="Z3";
                break;
            }
            case Z4:
            {
                zona="Z4";
                break;
            }
            case Z5:
            {
                zona="Z5";
                break;
            }
            }
            printf("Zona de pedido: %s \n",zona);
            lista_iter_avanzar(iter);
        }
        lista_iter_destruir(iter);
    }
    return true;
}

bool imprimir_enviados(lista_cola_t* lista_col)
{
    cola_t* cola_aux;
    datos_t* datos_aux;
    char* zona;
    cola_aux=cola_crear();
    if (cola_esta_vacia(lista_col->enviados))
    {
        printf("Hasta el momento no se han enviado pedidos.\n");
    }
    else
    {
        printf("Los pedidos enviados hasta el momento son:\n");
        while (!cola_esta_vacia(lista_col->enviados))
        {
            datos_aux=cola_desencolar(lista_col->enviados);
            printf("Pedido numero :%d \n",datos_get_numero(datos_aux));
            printf("Nombre del cliente:%s \n",datos_get_cliente(datos_aux));
            printf("Cantidad de pizzas pedidas: %d \n",datos_get_cantidad(datos_aux));
            switch(datos_get_zona(datos_aux))
            {
            case Z1:
            {
                zona="Z1";
                break;
            }
            case Z2:
            {
                zona="Z2";
                break;
            }
            case Z3:
            {
                zona="Z3";
                break;
            }
            case Z4:
            {
                zona="Z4";
                break;
            }
            case Z5:
            {
                zona="Z5";
                break;
            }
            }
            printf("Zona de pedido: %s \n",zona);
            cola_encolar(cola_aux,datos_aux);
        }
    }
    cola_destruir(lista_col->enviados,NULL);
    lista_col->enviados=cola_aux;
    return true;
}

bool imprimir_por_zona(lista_cola_t* lista_col)
{
    cola_t* cola_aux;
    datos_t* datos_aux;
    int zon;
    char* zona;
    cola_aux=cola_crear();
    printf("Ingrese la zona\n");
    printf("0=Z1; 1=Z2; 2=Z3; 3=Z4; 4=Z5 \n");
    scanf("%d", &zon);
    if (cola_esta_vacia(lista_col->cola_zonas[zon]))
    {
        printf("Hasta el momento no se han preparado pedidos para la zona seleccionada.\n");
    }
    else
    {
        printf("Los pedidos preparados hasta el momento para la zona seleccionada son:\n");
        while (!cola_esta_vacia(lista_col->cola_zonas[zon]))
        {
            datos_aux=cola_desencolar(lista_col->cola_zonas[zon]);
            printf("Pedido numero :%d \n",datos_get_numero(datos_aux));
            printf("Nombre del cliente:%s \n",datos_get_cliente(datos_aux));
            printf("Cantidad de pizzas pedidas: %d \n",datos_get_cantidad(datos_aux));
            switch(zon)
            {
            case Z1:
            {
                zona="Z1";
                break;
            }
            case Z2:
            {
                zona="Z2";
                break;
            }
            case Z3:
            {
                zona="Z3";
                break;
            }
            case Z4:
            {
                zona="Z4";
                break;
            }
            case Z5:
            {
                zona="Z5";
                break;
            }
            }
            printf("Zona de pedido: %s \n",zona);
            cola_encolar(cola_aux,datos_aux);
        }
    }
    cola_destruir(lista_col->cola_zonas[zon],NULL);
    lista_col->cola_zonas[zon]=cola_aux;
    return true;
}


datos_t* ingresar_datos(int numero)
{
    char cliente[20]="";
    char* clie=malloc(20*sizeof(char));
    int zon;
    int cant;
    bool est;
    printf("Ingrese nombre del cliente\n");
    scanf("%s", cliente);
    printf("Ingrese cantidad de pizzas\n");
    scanf("%d", &cant);
    fflush(stdin);
    if (cant>5 || cant<0) return NULL;
    printf("Ingrese zona de reparto\n");
    printf("Las opciones son 0=Z1,1=Z2,2=Z3,3=Z4,4=Z5\n");
    scanf("%d", &zon);
    fflush(stdin);
    if (zon>4 || zon<0) return NULL;
    est=false; /*  cambiar el estado  a true cuando se manda a preparar*/
    strcpy(clie,cliente);
    return nodo_datos_crear(cant,clie,zon,est,numero);
}

void lista_cola_destruir(lista_cola_t* lista_col)
{
    int i;
    lista_destruir(lista_col->pedidos,destructor_pedidos);
    cola_destruir(lista_col->enviados,destructor_pedidos);
    for(i=0; i<CANT_ZON; i++)
    {
        cola_destruir(lista_col->cola_zonas[i],destructor_pedidos);
    }
    free(lista_col->cola_zonas);
    free(lista_col);
}

bool ver_menu()
{
    lista_cola_t* lista_dia;
    bool centinela=true;
    int opcion;
    datos_t* datos;
    int nro_ped;
    lista_dia=lista_cola_crear();
    if (lista_dia==NULL) return NULL;
    nro_ped=0;
    while(centinela)
    {
        printf("Bienvenido a la pizzeria. Elija una opcion del menu.\n");
        printf("1:Ingresar pedido\n");
        printf("2:Modificar pedido\n");
        printf("3:Enviar pedidos\n");
        printf("4:Ver pedidos registrados\n");
        printf("5:Ver pedidos enviados por zona \n");
        printf("6:Ver pedidos enviados\n");
        printf("7:Ha finalizado el ingreso de pedidos por este dia\n");
        printf("8:Salir\n");
        scanf("%d",&opcion);
        fflush(stdin);
        switch(opcion)
        {
        case 1:
        {
            nro_ped++;
            datos=ingresar_datos(nro_ped);
	    if (!datos){
		nro_ped--;
		printf("Por favor,ingrese una opcion dentro de los rangos pedidos \n");
		break;}
	    
            ingresar_pedido(datos,lista_dia);
            preparar_pedido(lista_dia);
            break;
        }
        case 2:
        {
            printf("Si se quiere borrar un pedido cuando ingrese la cantidad de pizzas,ingrese 0 \n");
            datos=ingresar_datos(nro_ped);
	    if (!datos){
		printf("Por favor,ingrese una opcion dentro de los rangos pedidos \n");
		break;}
            modificar_pedido(lista_dia,datos,datos_get_cantidad(datos));
            break;
        }
        case 3:
        {
            enviar_pedido(lista_dia);
            break;
        }
        case 4:
        {
            imprimir_registros(lista_dia);
            break;
        }
        case 5:
        {
            imprimir_por_zona(lista_dia);
            break;
        }
        case 6:
        {
            imprimir_enviados(lista_dia);
            break;
        }
        case 7:
        {
            cerrar_caja(lista_dia);
            lista_cola_destruir(lista_dia);
            break;
        }
        case 8:
        {
            centinela=false;
            break;
        }
        default:
        {
            break;
        }

        }
    }
    return true;
}
