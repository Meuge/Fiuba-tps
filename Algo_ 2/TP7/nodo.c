#include "nodo.h"
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <memory.h>
#include <malloc.h>


typedef struct nodo
{
    struct nodo* siguiente;
    void* dato;
} t_nodo;

typedef struct datos
{
    int cantidad;
    char* cliente;
    zonas zona;
    bool estado;
    int numero;
} t_datos;


void destructor_pedidos(void* pedidos)
{
    free(((datos_t*)pedidos)->cliente);
    free((datos_t*)pedidos);
}

nodo_t* nodo_crear(const void* data)
{
    nodo_t* nodo = malloc(sizeof(nodo_t));
    if (nodo == NULL) return NULL;
    nodo->siguiente=NULL;
    nodo->dato=(datos_t*)data;
    return nodo;
}

void nodo_set_siguiente(nodo_t* nodo, nodo_t* nodo_sig)
{
    nodo->siguiente=nodo_sig;
}

nodo_t* nodo_get_siguiente(nodo_t* nodo)
{
    return nodo->siguiente;
}

datos_t* nodo_datos_crear(int cant,char* clie,zonas zon,bool est,int num)
{
    datos_t* data = malloc(sizeof(datos_t));
    if (data == NULL) return NULL;
    ((datos_t*)data)->cantidad=cant;
    ((datos_t*)data)->cliente=clie;
    ((datos_t*)data)->zona=zon;
    ((datos_t*)data)->estado=est;
    ((datos_t*)data)->numero=num;
    return data;
}

datos_t* nodo_get_dato(nodo_t* nodo)
{
    return (datos_t*)nodo->dato;
}

void datos_set_cliente(datos_t* data, char* clie)
{
    data->cliente=clie;
}

char* datos_get_cliente(const datos_t* data)
{
    return (char*)data->cliente;
}

void datos_set_cantidad(datos_t* data, int cant)
{
    data->cantidad=cant;
}

int datos_get_cantidad(datos_t* data)
{
    return data->cantidad;
}

void datos_set_zona(datos_t* data, zonas zon)
{
    data->zona=zon;
}

zonas datos_get_zona(datos_t* data)
{
    return data->zona;
}

void datos_set_estado(datos_t* data, bool est)
{
    data->estado=est;
}

bool datos_get_estado(datos_t* data)
{
    return data->estado;
}

void datos_set_numero(datos_t* data, int num)
{
    data->numero=num;
}

int datos_get_numero(datos_t* data)
{
    return data->numero;
}
