from archivo import  leer_archivotexto
import grafo, algoritmos_grafos
from grafo import Grafo




def probar(linea,valor_1, valor_2,bool):
		#Se utiliza para verificar la validez de dos expresiones
    	if  bool:  comp = (valor_1 == valor_2) 
    	else: comp = (valor_1 != valor_2)	
    	if comp:
        	print linea, "OK"
    	else:
        	print linea,"ERROR"


def pruebas_lectura():
	"""Comprueba la lectura de archivos"""
	print("Prueba leer el archivo")
	grafo_nodos,grafo_calles,nodo_info=leer_archivotexto("partido_gerli.txt")
	probar("Prueba lectura de archivo grafo_nodos",grafo_nodos.__contains__("1"),True,True) 
	probar("Prueba lectura de archivo grafo_calles",grafo_calles.__contains__("pilcomayo"),True,True)

def pruebas_grafo():	
	"""Realiza operaciones sobre el grafo, comprueba los metodos de la clase Grafo"""
	print("Prueba crear un grafo")
	grafo_nodos=Grafo()
		
	#Grafo a realizar
	"""
              <-------------------------------------+
              1+------------------------------------+>2<------
              + <--------------------+                +      | 
              |                      |                |      |
              |                      |                |      |
              |                      |                |      |
              |                      |                |      |
              |                      +                |      | 
              |+-------------------->6                |      |
              ||                                      |      |
              ||                                      |      |
              ||  									  v	 	 |	
              ||									  5-------
              ||                                      +
              ||                                      |
              v| 									  |			
              3++--------------------->4---------------

	"""



	print("Agrega vertice 1")
	grafo_nodos.agregar_vertice("1")
	print("Agrega vertice 2")
	grafo_nodos.agregar_vertice("2")
	print("Agrega vertice 3")
	grafo_nodos.agregar_vertice("3")
	print("Agrega vertice 6")
	grafo_nodos.agregar_vertice("6")
	print("Agrega peso a las aristas estan son de acuerdo si es avenida o calle principal,secundaria")
	print("Agrega peso 1 al vertice 1 y 2")
	grafo_nodos.agregar_arista("1","2","1")
	print("Agrega peso 1 al vertice 2 y 1")
	grafo_nodos.agregar_arista("2","1","1")
	print("Agrega peso 1 al vertice 1 y 3")
	grafo_nodos.agregar_arista("1","3","1")
	print("Agrega peso 2 al vertice 3 y 4")
	grafo_nodos.agregar_arista("3","4","2")
	print("Agrega peso 2 al vertice 2 y 5")
	grafo_nodos.agregar_arista("2","5","2")
	print("Agrega peso 1 al vertice 6 y 4")
	grafo_nodos.agregar_arista("6","4","1")
	print("Agrega peso 1 al vertice 6 y 3")
	grafo_nodos.agregar_arista("6","3","1")
	print("Agrega peso 1 al vertice 1 y 6")
	grafo_nodos.agregar_arista("1","6","1")
	print("Agrega peso 1 al vertice 6 y 1")
	grafo_nodos.agregar_arista("6","1","1")
	print("Agrega peso 1 al vertice 3 y 6")
	grafo_nodos.agregar_arista("3","6","1")
	print("Agrega peso 1 al vertice 3 y 4")
	grafo_nodos.agregar_arista("3","4","1")
	print("Agrega peso 1 al vertice 4 y 5")
	grafo_nodos.agregar_arista("4","5","1")
	print("Agrega peso 1 al vertice 5 y 2")
	grafo_nodos.agregar_arista("5","2","1")

	probar("Prueba los vertices que tiene el grafo",sorted(grafo_nodos.obtener_vertices()),["1","2","3","4","5","6"],True)
	vertice_1=grafo_nodos.obtener_vertice("1")
	vertice_2=grafo_nodos.obtener_vertice("2")
	vertice_3=grafo_nodos.obtener_vertice("3")
	vertice_6=grafo_nodos.obtener_vertice("6")
	probar("Prueba las conexiones del vertice 1",sorted(vertice_1.conseguir_conexiones()),sorted([vertice_2,vertice_3,vertice_6]),True)
	probar("Prueba el peso de la conexion del vertice 1 con vertice 2",vertice_1.conseguir_peso(vertice_2),"1",True)
	camino_ida=algoritmos_grafos.fijar_distancia_nodos(grafo_nodos,vertice_1)
	for (i,j) in camino_ida.items():
		print i.conseguir_clave(),j.conseguir_clave(),"camino_ida,nodo,padre"
	camino_vuelta=algoritmos_grafos.fijar_distancia_nodos(grafo_nodos,vertice_3)
	for (i,j) in camino_vuelta.items():
		print i.conseguir_clave(),j.conseguir_clave(),"camino_vuelta,nodo,padre"

	trayecto_1=algoritmos_grafos.camino_optimo(camino_ida,vertice_1,vertice_3)
	print("Imprime ruta de trayecto del vertice 1 al vertice 3")
	algoritmos_grafos.imprimir_trayecto(trayecto_1)
	trayecto_2=algoritmos_grafos.camino_optimo(camino_vuelta,vertice_3,vertice_1)
	print("Imprime ruta de trayecto del vertice 3 al vertice 1")
	algoritmos_grafos.imprimir_trayecto(trayecto_2)

def formato_calles(cadena):
	""" Otorga el mismo formato a las calles que el del archivo"""
	lista=[]
	lista=cadena.split()
	calle_1=""
	calle_2=""
	for i,j in enumerate(lista):
		if j=="y":
			index=i
	calle_1=lista[:index]
	calle_2=lista[index+1:]
	calle_1=" ".join(calle_1)
	calle_2=" ".join(calle_2)
	return calle_1,calle_2


def pruebas_validacion():
	"""Comprueba la entrada de datos"""
	print("Comprueba que se ingrese en el formato pedido las calles")
	calle1,calle2=formato_calles("pueyrredon y santa fe")
	cad_1="pueyrredon"
	cad_2="santa fe"

	if calle1==cad_1:
			print ("OK CALLE 1")
	if calle2==cad_2:
		print ("OK CALLE 2")


pruebas_lectura()
pruebas_grafo()
pruebas_validacion()







	


 
