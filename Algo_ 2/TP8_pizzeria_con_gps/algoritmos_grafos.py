from grafo import Grafo
from grafo import Vertice





def buscar_minimo(distancia,visitados):
    """ Busca el nodo con menor distancia"""
    minimo=float("inf")
    vertice_objeto=""
    for vertice,estado in visitados.items():
                if distancia[vertice]<=minimo and estado==False:
                    minimo=distancia[vertice]
                    vertice_objeto=vertice
    return vertice_objeto

def fijar_distancia_nodos(grafo,nodo_inicial):
    """Fija distancia a los nodos del grafo, a partir del nodo inicial"""
    
    todos_los_caminos = {} #contiene todos los caminos posibles del nodo inicial al final
    distancia_nodos = {} #establece la mejor distancia entre un par de nodos, contiene la relacion, sucesor, predecesor
    visitados = {} #contiene los nodos que van siendo visitados
    infinito=float("inf")
    for vertice in grafo.obtener_vertice_objetos():
        distancia_nodos[vertice] = infinito #Marco los nodos con distancia infinia
        visitados[vertice] = False #Marco los nodos como no visitados
        if vertice in nodo_inicial.conseguir_conexiones(): ##Marco quienes son los vecinos del nodo inicial, el peso depende si es avenida o calle
            distancia_nodos[vertice] = float(nodo_inicial.conseguir_peso(vertice))
            todos_los_caminos[vertice]=nodo_inicial #Marco el padre de las conexiones del vertice incial

    distancia_nodos[nodo_inicial] = 0 #El nodo inicial tiene distancia cero
    visitados[nodo_inicial]=True
    while False in visitados.values():
                        vertice=buscar_minimo(distancia_nodos,visitados) #busco el que se encuentra a minima distancia en el grafo
                        visitados[vertice] = True
                       
                       
                        for elemento in vertice.conseguir_conexiones():


                                if float(distancia_nodos[elemento]) > float(distancia_nodos[vertice]) + float(vertice.conseguir_peso(elemento)):#si la distancia es mejorada se modifica
                                    
                                
                                    distancia_nodos[elemento] = float(distancia_nodos[vertice]) + float(vertice.conseguir_peso(elemento)) #modifica la distancia mejorada
                                    todos_los_caminos[elemento] = vertice #indica quien es el predecesor


                               
               
                   
    return todos_los_caminos

def camino_optimo(camino, vertice_inicial, vertice_final):
    """Retorna el camino del nodo inicial al final"""
            
    recorrido=[]
    temporal = camino[vertice_final]
    recorrido.append(vertice_final)
    
    for i,j in camino.items():
            recorrido.append(temporal)
            if temporal==vertice_inicial: #concidion de corte es que el padre sea el vertice inicial
                break
            temporal=camino[temporal] #adjunta padre
 
    recorrido.reverse()
 
    return recorrido

def imprimir_trayecto(trayecto):
    for i in trayecto:
        print i



def nodo_interseccion(grafo,calle_1,calle_2):
    """Busca el nodo en el cual se produce la interseccion de las calles"""
    """ El usario tiene que ingresar una interseccion de calles"""
    """Como las calles cuando se intersectan lo hacen en dos tramos hay dos nodos de tal interseccion, elije el primero que encuentra"""
    vertice_1=grafo.obtener_vertice(calle_1)
    vertice_2=grafo.obtener_vertice(calle_2)
    for k in vertice_1.conseguir_conexiones():
        for j in vertice_2.conseguir_conexiones():
          if vertice_1.conseguir_peso(k)== j.conseguir_clave():
                #print "El nodo interseccion de las calles es ", j.conseguir_clave() 
                return j.conseguir_clave()
           

def conseguir_latitud_longitud(nodo,diccionario_nodos):
    """Retorna la latitud y longitud del nodo""" 
    return diccionario_nodos[nodo]['latitud'],diccionario_nodos[nodo]['longitud']

def conseguir_x_y(nodo,diccionario_nodos):
    """Retorna la coordenada x e y del nodo"""
    return diccionario_nodos[nodo]['x'], diccionario_nodos[nodo]['y']

