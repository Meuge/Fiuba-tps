import csv,os
from grafo import Grafo

def archivo_texto():
	"""Recibe el archivo por parte del usuario valida que no este vacio y que el mismo exista"""
	encontrado=False
	while encontrado==False:
		try:
			arch=raw_input("Ingrese nombre del archivo:")
			print "\n"
			arch=str(arch)
			size=os.path.getsize(arch)
			if size>0:
					encontrado=True
					return arch
			else:
					print "El archivo esta vacio","\n"
					encontrado=False
		except OSError:
					print "Nombre no valido","\n"
		except EOFError:
					print "Nombre no valido","\n"


def leer_archivotexto(texto):
		""" Se lee el archivo"""
		grafo_nodos=Grafo()
		grafo_calles=Grafo()
		nodos_info={};
		archivo =open(texto,"r")
		i=0
		archivo_csv=csv.reader(archivo,delimiter=",")
		contador=(archivo_csv.next()) #Saltea la primer linea
		primera_parte=True
		for row in archivo_csv:
			i=i+1
			if i<int(contador[0])+1:
				nodos_info[row[0]] = {'x': (row[1]), 'y': (row[2]), 'latitud': (row[3]),'longitud': (row[4])} #Guardo info de nodos para kml
			if i>int(contador[0])+1:
				if row[4] not in grafo_nodos:
					grafo_nodos.agregar_vertice(row[4])
				if row[1].lower() not in grafo_calles:
					grafo_calles.agregar_vertice(row[1].lower())
				grafo_nodos.agregar_arista(row[4],row[5],row[2])
				grafo_calles.agregar_arista(row[1].lower(),row[4],row[5])
				if int(row[3])==0: #En caso de que sea doble mano se agrega la adyacencia de uno al otro y viceversa
						if row[5] not in grafo_nodos:
							grafo_nodos.agregar_vertice(row[5])
						grafo_nodos.agregar_arista(row[5],row[4],row[2])
		

	

		archivo.close()
		return grafo_nodos,grafo_calles,nodos_info






