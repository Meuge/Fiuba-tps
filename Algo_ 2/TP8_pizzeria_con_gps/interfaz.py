# coding: utf8
import archivo
import algoritmos_grafos
from grafo import Grafo
import archivo_kml
from archivo_kml import Kml


def validar_calles():
	"""Pide al usuario que ingrese las calles"""
	while True:
			calle=raw_input("Ingrese interseccion de calles: ")
			sin_espacios=calle.replace(" ","")
			lista=calle.split() 
			centinela=False
			letras_verificadas=False
			for i in lista: #Podria haber mas de una ocurrencia de "y"
				if i=="y":
					centinela=True
			for i in sin_espacios:
				if i.isalnum() or i==".": #Hay  calles que contienen numero, letras o propiamente un punto como ("av.")para avenida
					letras_verificadas=True
				else:
					letras_verificadas=False
					break
			if letras_verificadas and centinela:
			
				return calle.lower()
			else:
				print "Ingrese caracteres en el formato requerido\n"


def formato_calles(cadena):
	""" Otorga el mismo formato a las calles que el del archivo"""
	lista=[]
	lista=cadena.split()
	calle_1=""
	calle_2=""
	for i,j in enumerate(lista):
		if j=="y":
			index=i
	calle_1=lista[:index]
	calle_2=lista[index+1:]
	calle_1=" ".join(calle_1)
	calle_2=" ".join(calle_2)
	return calle_1,calle_2


def verificar_calles_en_archivo(grafo_calles):
	"""Verifica que las calles se encuentren en el archivo de nodos"""
	while True:
		direccion=validar_calles()
		calle_1,calle_2=formato_calles(direccion)
		vertice_1=grafo_calles.obtener_vertice(calle_1)
		vertice_2=grafo_calles.obtener_vertice(calle_2)
		if vertice_1 and vertice_2:
					return calle_1,calle_2
		else:
					print " Ingrese calles validas en el formato requerido\n"

def armar_kml(ruta_kml,nodo_diccionario,nombre,lista_nodos):
	"""Consigue la latitud y longitud de cada nodo que conforma el recorrido, crea un archivo kml"""
	cadena_kml=""
	archivo_kml=Kml(nombre) #Crea el Objeto kml
	archivo_kml.encabezado() #Realiza el encabezado del kml
	for i in range(len(ruta_kml)): #Itera en todos los trayectos realizados
			cadena_kml=conseguir_ruta_kml(nodo_diccionario,ruta_kml[i])
			nombre="trayecto_"+str(i)
			archivo_kml.linea(cadena_kml,nombre)
	for i in range(len(lista_nodos)): #Itera en la lisa de nodos para realizar un marcador en el kml
		latitud,longitud=algoritmos_grafos.conseguir_latitud_longitud(lista_nodos[i],nodo_diccionario)
		if i==0:
			archivo_kml.agregar_marcadores("Pizzeria",latitud,longitud) #Se agrega el marcador llama pizzeria
		else:
			cadena="cliente"+str(i)
			archivo_kml.agregar_marcadores(cadena,latitud,longitud) #Marcadores de clientes

	archivo=archivo_kml.pie_documento()

def fijar_distancia(ruta,nodo_diccionario):
	"""Imprime y fija la distancia del recorrido """
	x=0
	y=0
	for i in ruta:
		x,y=algoritmos_grafos.conseguir_x_y(i.conseguir_clave(),nodo_diccionario)
		x+=x
		y+=y
	print ("La distancia con respecto a x es",x)
	print ("La distancia con respecto a y es",y)
	print"\n"




def conseguir_ruta_kml(nodo_diccionario,lista):
	"""Consigue la ruta del recorrido con la respectiva latitud y longitud de cada nodo"""
	cadena_kml=""
	for j in lista:
			latitud,longitud=algoritmos_grafos.conseguir_latitud_longitud(j.conseguir_clave(),nodo_diccionario)
			cadena_kml=cadena_kml+latitud
			cadena_kml=cadena_kml + ','
			cadena_kml=cadena_kml + longitud +' '
	return cadena_kml

def eleccion_1(nodo_inicial,lista_nodos,grafo_calles,grafo_nodos,nodo_diccionario):
	"""Ejecuta las funciones para que la pizzeria lleve a cabo la entrega del pedido"""
	while True:
		print("A continuacion se le requerira a donde desea llegar\n")
		calle_1_cliente,calle_2_cliente=verificar_calles_en_archivo(grafo_calles)
		nodo_final=algoritmos_grafos.nodo_interseccion(grafo_calles,calle_1_cliente,calle_2_cliente)
		if nodo_final:
			rutas_kml=eleccion_tareas_comun(nodo_inicial,nodo_final,grafo_nodos,nodo_diccionario)
			lista_nodos.append(nodo_final)
			break
		else:
			print "Por favor,ingrese interseccion de calles"

	return rutas_kml,lista_nodos
	

def eleccion_tareas_comun(nodo_inicial,nodo_final,grafo_nodos,nodo_diccionario):
	"""Ejecuta las tareas que son comunes para cuando un cliente obtiene una pizza que no"""
	"""es la encargada, como para cuando la pizzeria realiza una entrega"""
	rutas_kml=[]
	
	nodo_final=grafo_nodos.obtener_vertice(nodo_final)
	nodo_inicial=grafo_nodos.obtener_vertice(nodo_inicial)
	print "Calculando ruta.."
	camino_ida=algoritmos_grafos.fijar_distancia_nodos(grafo_nodos,nodo_inicial)
	camino_vuelta=algoritmos_grafos.fijar_distancia_nodos(grafo_nodos,nodo_final)
	trayecto_1=algoritmos_grafos.camino_optimo(camino_ida,nodo_inicial,nodo_final)
	trayecto_2=algoritmos_grafos.camino_optimo(camino_vuelta,nodo_final,nodo_inicial)
	print("La distancia en el recorrido de ida es:")
	fijar_distancia(trayecto_1,nodo_diccionario)
	print ("La distancia en el recorrido de vuelta es:")
	fijar_distancia(trayecto_2,nodo_diccionario)
	rutas_kml.append(trayecto_1) #agrega el trayecto de ida para realizar el kml
	rutas_kml.append(trayecto_2) #agrega el trayecto de vuelta para realizar el kml
	return rutas_kml


def eleccion_2(lista_nodos,grafo_calles,grafo_nodos,nodo_diccionario):
	"""Ejecuta la opcion del sistema que es caundo se desea intercambiar pizzas"""
	while True:
		print("A continuacion se le requerira la interseccion de calles en donde se encuentra\n")
		calle_1_cliente,calle_2_cliente=verificar_calles_en_archivo(grafo_calles)
		print("A continuacion se le requerira a donde desea ir\n")
		calle_1_cliente_1,calle_2_cliente_2=verificar_calles_en_archivo(grafo_calles) #verifica el ingreso de datos
		nodo_inicial=algoritmos_grafos.nodo_interseccion(grafo_calles,calle_1_cliente,calle_2_cliente) #crea el nodo del cliente
		nodo_final=algoritmos_grafos.nodo_interseccion(grafo_calles,calle_1_cliente_1,calle_2_cliente_2) #crea el nodo de otro cliente
		if nodo_inicial and nodo_final:

			ruta_kml=eleccion_tareas_comun(nodo_inicial,nodo_final,grafo_nodos,nodo_diccionario)
			lista_nodos.append(nodo_inicial) #agrega el nodo de un cliente
			lista_nodos.append(nodo_final) #agrega el nodo de otro cliente
			break
		else:
			print ("Por favor,ingrese interseccion de calles")

	return ruta_kml,lista_nodos
	
def ingresar_direccion_pizzeria(grafo_calles):
	"""Pide el ingreso de la direccion de la pizerria y verifica que exista un nodo interseccion"""
	while True:
		calle_1_pizzeria,calle_2_pizzeria=verificar_calles_en_archivo(grafo_calles)
		nodo_inicial=algoritmos_grafos.nodo_interseccion(grafo_calles,calle_1_pizzeria,calle_2_pizzeria)
		if nodo_inicial:
			return nodo_inicial
		else:
			print("Por favor, ingrese una interseccion de calles:")

def interface():
	"""Interacciona con el usuario"""
	lista_nodos_1=[]
	rutas_kml_1=[]
	rutas_kml_2=[]
	lista_nodos_2=[]
	archivo_pedidos="pedidos.kml"
	archivo_clientes="clientes.kml"
	nombre_archivo=archivo.archivo_texto()
	grafo_nodos,grafo_calles,nodo_diccionario=archivo.leer_archivotexto(nombre_archivo)
	print"Bienvenido al sistema de gps de la pizzeria de Gerli"
	print"Cada vez que ingrese una interseccion de calles, debera ingresar la primer calle seguido de", "y", "y luego la segunda calle"
	print"A continuacion se le requerira que ingrese direccion de la pizzeria:","\n","\n"
	nodo_inicial=ingresar_direccion_pizzeria(grafo_calles)
	lista_nodos_1.append(nodo_inicial)
	lista_nodos_2.append(nodo_inicial)
	while True:
		print("1: Para ingresar pedidos")
		print("2: Para queja de cliente por intercambio de pizzas")
		print("3: Para salir del sistema")
		eleccion=raw_input("Ingrese una opcion del menu:")
		
		if eleccion=="1":
			recorrido_kml_1,lista_nodos_1=eleccion_1(nodo_inicial,lista_nodos_1,grafo_calles,grafo_nodos,nodo_diccionario)
			rutas_kml_1.extend(recorrido_kml_1)
		if eleccion=="2":
			recorrido_kml_2,lista_nodos_2=eleccion_2(lista_nodos_2,grafo_calles,grafo_nodos,nodo_diccionario)
			rutas_kml_2.extend(recorrido_kml_2)
		if eleccion=="3":
			if rutas_kml_1:
				armar_kml(rutas_kml_1,nodo_diccionario,archivo_pedidos,lista_nodos_1)
			if len(rutas_kml_2):
				armar_kml(rutas_kml_2,nodo_diccionario,archivo_clientes,lista_nodos_2)
			break
		else:
			print "Ingrese algun numero mostrado en el menu"

interface()


