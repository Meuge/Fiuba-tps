#include "abb.h"
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>

/* ******************************************************************
 *                        PRUEBAS UNITARIAS
 * *****************************************************************/

/* Función auxiliar para imprimir si estuvo OK o no. */
void print_test(char* name, bool result, int valor)
{
    printf("%s: %s %d\n", name, result? "OK" : "ERROR", valor);
}

/* Prueba que las primitivas del arbol funcionen correctamente. */

int comparame (const char* clave1, const char* clave2){
		return strcmp(clave1,clave2);
}

bool imprimir(const char* clave, void* dato, void* extra){
    printf("clave: %s valor: %s extra: %d\n", clave, (char*)dato, *(int*)extra);
    return true;
}

void prueba_abb()
{
char *var1="Riquelme";
char *var2="Palermo";
char *var3="Superclasico";
char *var4="Melli";
char *var8="Blandi es de Campana";
char *var9="Palacios";
char *var11="CADU";
char *var12="Villa Dalmine";
/*char *var10="Arruabarrena";
char *var13="Cagna";
char *var5="Battaglia";
char *var6="Schiavi";
char *var7="Clemente";*/
abb_iter_t* iter;
int extra;

abb_t* arbol_1 = abb_crear(*comparame,NULL);
print_test("Prueba que el arbol este vacio: ", arbol_1,1);
print_test("Cantidad del arbol es : ", abb_cantidad(arbol_1) == 0,1);
print_test("Guardo clave 5 con dato Riquelme:",abb_guardar(arbol_1, "5", var1),1);
print_test("Cantidad de arbol es : ", abb_cantidad(arbol_1) == 1,1);
print_test("Guardo clave 9, dato Palermo en arbol: ", abb_guardar(arbol_1, "9", var2),1);
print_test("Guardo clave 2, dato Superclasico en arbol: ", abb_guardar(arbol_1, "2", var3),1);
print_test("Guardo clave 7, dato Melli en arbol: ", abb_guardar(arbol_1, "7", var4),1);
print_test("Reemplazo dato de clave 7 por Palacios en arbol: ", abb_guardar(arbol_1, "7",var9), 1);
print_test("Guardo clave 1, dato CADU en arbol: ", abb_guardar(arbol_1, "1", var11),1);
print_test("Reemplazo dato de clave 1 por Villa Dalmine en arbol: ", abb_guardar(arbol_1, "1", var12),1);
print_test("Reemplazo dato de clave 9 por Blandi en arbol: ", abb_guardar(arbol_1, "9", var8),1);
print_test("Cantidad de arbol es : ", abb_cantidad(arbol_1) == 5,5);
print_test("Busco clave 5 con abb_pertenece: ",abb_pertenece(arbol_1, "5"),1);
print_test("Busco clave 19999 con abb_pertenece: ",!abb_pertenece(arbol_1, "19999"),1);
print_test("Busco clave 777 con abb_obtener ", NULL == abb_obtener(arbol_1, "777"),1);
print_test("Busco clave 5 con abb_obtener ", var1 == abb_obtener(arbol_1, "5"),1);
print_test("Busco clave 9 con abb_obtener  ", var8 == abb_obtener(arbol_1, "9"),1);
print_test("Borro nodo con clave 1 , Villa Dalmine: ", abb_borrar(arbol_1, "1")==var12,1);
print_test("Cantidad de arbol es : ", abb_cantidad(arbol_1) == 4,4);
print_test("Borro nodo con clave 2, superclasico: ", abb_borrar(arbol_1, "2") == var3,1);
print_test("Borro nodo con clave 5, Riquelme: ", abb_borrar(arbol_1, "5") == var1 ,1);
print_test("Borro nodo con clave 7, Palacio: ", abb_borrar(arbol_1, "7") == var9,1);
print_test("Borro nodo con clave 9, superclasico: ", abb_borrar(arbol_1, "9") == var8,1);
print_test("Cantidad de arbol es 0: ", abb_cantidad(arbol_1) == 0,0);
print_test("Borro nodo con clave 9, superclasico: ", abb_borrar(arbol_1, "9") == NULL,1);
print_test("Busco clave 888 con abb_obtener ", NULL == abb_obtener(arbol_1, "888"),1);
iter=abb_iter_in_crear(arbol_1);
while(!abb_iter_in_al_final(iter)){
    printf("%s\n",abb_iter_in_ver_actual(iter));
    abb_iter_in_avanzar(iter);
}
abb_iter_in_destruir(iter);
extra=2;
abb_in_order(arbol_1,*imprimir,&extra);
abb_destruir(arbol_1);
}

/* ******************************************************************
 *                        PROGRAMA PRINCIPAL
 * *****************************************************************/


int main(void) {
    prueba_abb();
    return 0;
}
