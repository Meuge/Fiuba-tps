#ifndef ABB_H
#define ABB_H

#include <stdbool.h>
#include <stddef.h>


/* ******************************************************************
 *                DEFINICION DE LOS TIPOS DE DATOS
 * *****************************************************************/
typedef struct nodo_abb {
    char* clave;
    void* dato;
    struct nodo_abb* izq;
    struct nodo_abb* der;
    struct nodo_abb* padre;
}nodo_abb_t;

typedef int (*abb_comparar_clave_t) (const char *, const char *);
typedef void (*abb_destruir_dato_t) (void *);

typedef struct abb{
	nodo_abb_t* raiz;
	size_t cantidad;
	abb_comparar_clave_t cmp;
	abb_destruir_dato_t destruir_dato;
}abb_t;

typedef struct abb_iter{
    const abb_t* arbol;
    nodo_abb_t* actual;
}abb_iter_t;



/* ******************************************************************
 *                    PRIMITIVAS DEL ABB
 * *****************************************************************/
 /*Crea el arbol*/
abb_t* abb_crear(abb_comparar_clave_t cmp, abb_destruir_dato_t destruir_dato);


/*Guarda el dato consu clave en el abb
Pre: El arbol fue creado
Post:Se inserta el dato devuelve verdadero, en caso de ya existir se reemplaza el dato*/
bool abb_guardar(abb_t *arbol, const char *clave, void *dato);

/*Borra el dato a suprimir del arbol
Pre: El arbol fue creado
Post:Se borra el dato en caso de no encontrarse devuelve NULL*/
void *abb_borrar(abb_t *arbol, const char *clave);


/*Devuelve el dato de la clave que se busca
Pre:El arbol fue creado
Post:Devuelve el dato, en caso de no hallarse devuelve NULL*/
void *abb_obtener(const abb_t *arbol, const char *clave);

/*Devuelve verdadero o falso en caso de que la clave a buscar pertenezca o no al arbol
Pre: El arbol fue creado
Post:Devuelve verdadero si halla la clave, falso en caso negativo */

bool abb_pertenece(const abb_t *arbol, const char *clave);

/*Devuelve la cantidad de elementos insertados
Pre:El arbol fue creado
Post: Devuelve la cantidad de elementos*/
size_t abb_cantidad(abb_t *arbol);

/*Destruye el arbol con sus datos
Pre: El arbol fue creado
Post: Se destruye cada dato y luego la estrcutura de ABB */
void abb_destruir(abb_t *arbol);

abb_iter_t *abb_iter_in_crear(const abb_t *arbol);

bool abb_iter_in_avanzar(abb_iter_t *iter);

const char *abb_iter_in_ver_actual(const abb_iter_t *iter);

bool abb_iter_in_al_final(const abb_iter_t *iter);

void abb_iter_in_destruir(abb_iter_t* iter);

void abb_in_order(abb_t *arbol, bool funcion(const char *, void *, void *), void *extra);

#endif
