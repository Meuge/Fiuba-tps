#ifndef LISTA_H
#define LISTA_H

#include <stdbool.h>
#include <stddef.h>

/* ******************************************************************
 *                DEFINICION DE LOS TIPOS DE DATOS
 * *****************************************************************/

/* La lista está planteada como una lista de punteros genéricos. */

typedef struct lista lista_t;
typedef struct iterador lista_iter_t;

/* ******************************************************************
 *                    PRIMITIVAS DE LA LISTA
 * *****************************************************************/

/* Crea una lista
 Post: devuelve una nueva lista vacía.*/
lista_t* lista_crear();

/* Devuelve verdadero o falso, según si la lista tiene o no elementos
 Pre: la lista fue creada.*/

bool lista_esta_vacia(const lista_t *lista);

/* Agrega un nuevo elemento a la lista. Devuelve falso en caso de error.
 Pre: la lista fue creada.
 Post: se agregó un nuevo elemento a la lista, dato es el primero de la misma.*/
bool lista_insertar_primero(lista_t *lista, void *dato);

/* Agrega un nuevo elemento a la lista. Devuelve falso en caso de error.
 Pre: la lista fue creada.
 Post: se agregó un nuevo elemento a la lista, dato es el ultimo de la misma.*/
bool lista_insertar_ultimo(lista_t *lista, void *dato);

/* Saca el primer elemento de la lista. Si la lista tiene elementos, se quita el
 primero de la lista, y se devuelve su valor, si está vacía, devuelve NULL.
 Pre: la lista fue creada.
 Post: se devolvió el valor del primer elemento anterior, la lista
 contiene un elemento menos, si la lista no estaba vacía.*/
void *lista_borrar_primero(lista_t *lista);

/* Obtiene el valor del primer elemento de la lista. Si la lista tiene
 elementos, se devuelve el valor del primero, si está vacía devuelve NULL.
 Pre: la lista fue creada.
 Post: retorna el primer elemento de la lista.*/
void *lista_ver_primero(const lista_t *lista);

/*Obtiene el largo de la lista.
Pre: la lista fue creada.
Post: retorna la cantidad de elementos de la lista.*/
size_t lista_largo(const lista_t *lista);

/* Destruye la lista. Si se recibe la función destruir_dato por parámetro,
 para cada uno de los elementos de la lista llama a destruir_dato.
 Pre: la lista fue creada. destruir_dato es una función capaz de destruir
 los datos de la lista.
 Post: se eliminaron todos los elementos de la lista.*/
void lista_destruir(lista_t *lista, void destruir_dato(void *));

/* ******************************************************************
 *                    PRIMITIVAS DEL ITERADOR
 * *****************************************************************/

/* Crea una lista
 Post: devuelve una nueva lista vacía.*/
lista_iter_t *lista_iter_crear(const lista_t *lista);

/* Avanza en un lugar la posicion actual de la lista. Devuelve verdadero o falso segun se pudo
 realizar la operacion de avanzar o no
 Pre: el iterador fue creado.
 Post: se avanzo en un lugar el actual.*/
bool lista_iter_avanzar(lista_iter_t *iter);

/* Devuelve el elemento al que hace referencia el actual o NULL en caso de que el actual no contenga datos.
 Pre: el iterador fue creado.
 Post: retorna el valor actual.*/
void *lista_iter_ver_actual(const lista_iter_t *iter);

/* Avanza al siguente elemento de la lista. Devuelve false si quedan elementos al seguier avanzando y
 verdadero si se alcanzo el final de la lista.
 Pre: el iterador esta creado.
 Post: la lista se recorre hasta el final.*/
bool lista_iter_al_final(const lista_iter_t *iter);

/* Destruye el iterador.
 Pre: el iterador fue creado.
 Post: se eliminaron todos los elementos del iterador*/
void lista_iter_destruir(lista_iter_t *iter);

/* ******************************************************************
 *                    PRIMITIVAS MIXTAS
 * *****************************************************************/

/* Agrega un nuevo elemento a la lista a continuacion del actual. Devuelve falso en caso de error,
 verdadero en caso contrario.
 Pre: la lista fue creada.
 Post: se agregó un nuevo elemento a la lista. el actual quedo en la misma posicion*/
bool lista_insertar(lista_t *lista, lista_iter_t *iter, void *dato);

/* Saca el elemento actual de la lista. Si la lista tiene elementos, se quita el
 actual de la lista, y se devuelve su valor, si está vacía, devuelve NULL.
 Pre: la lista fue creada.
 Post: se devolvió el valor del elemento actual, si la lista no estaba vacía, la lista
 contiene un elemento menos. El actual paso a ser el anterior .*/
void *lista_borrar(lista_t *lista, lista_iter_t *iter);

/* Devuelve el siguiente elemento de la lista. Si el elemento siguiente no existe se devuelve NULL.
 Pre: la lista fue creada.
 Post: retorna el valor del elemento siguiente al actual.*/
void* lista_ver_siguiente(lista_iter_t *iter);


#endif /* LISTA_H*/
