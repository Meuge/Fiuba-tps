#include "lista.h"
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <memory.h>

typedef struct nodo
{
    void * data;
    struct nodo *siguiente;
} nodo_t;

typedef struct lista
{
    nodo_t* primero;
    nodo_t* ultimo;
    size_t tamanio;
} t_lista;

typedef struct iterador
{
    nodo_t* anterior;
    nodo_t* actual;
} t_lista_iter;

nodo_t* nodo_crear()
{
    nodo_t* nodo = malloc(sizeof(nodo_t));
    if (nodo == NULL) return NULL;
    return nodo;
}

lista_t* lista_crear()
{
    lista_t* lista=malloc(sizeof(lista_t));
    if (lista == NULL) return NULL;
    lista->primero=NULL;
    lista->ultimo=NULL;
    lista->tamanio=-1;
    return lista;
}

bool lista_esta_vacia(const lista_t *lista)
{
    if (lista->primero==NULL)return true;
    return false;
}

bool lista_insertar_primero(lista_t *lista, void *dato)
{
    nodo_t *nodo_aux;
    nodo_aux=nodo_crear();
    if (nodo_aux==NULL) return false;
    nodo_aux->data=dato;
    lista->tamanio++;
    nodo_aux->siguiente=lista->primero;
    lista->primero=nodo_aux;
    if (lista->ultimo==NULL)
        lista->ultimo=nodo_aux;
    return true;

}

bool lista_insertar_ultimo(lista_t *lista, void *dato)
{
    nodo_t *nodo_aux;
    nodo_aux=nodo_crear();
    if (nodo_aux==NULL) return false;
    nodo_aux->data=dato;
    nodo_aux->siguiente=NULL;
    lista->tamanio++;
    if (lista->ultimo==NULL)
    {
        lista->primero=nodo_aux;
        lista->ultimo=nodo_aux;
    }
    else
    {
        lista->ultimo->siguiente=nodo_aux;
        lista->ultimo=nodo_aux;
    }
    return true;

}
void *lista_borrar_primero(lista_t *lista)
{
    nodo_t* nodo_aux;
    void *dato_aux;
    if (lista_esta_vacia(lista))return NULL;
    dato_aux=(lista->primero)->data;
    lista->tamanio-=1;
    nodo_aux=lista->primero;
    if (lista->primero==lista->ultimo)
    {
        lista->primero=NULL;
        lista->ultimo=NULL;
    }
    else
    {

        lista->primero=lista->primero->siguiente;
    }
    free(nodo_aux);
    return dato_aux;
}

void *lista_ver_primero(const lista_t *lista)
{
    void *dato;
    if(lista_esta_vacia(lista))return NULL;
    dato=(lista->primero)->data;
    return dato;
}

size_t lista_largo(const lista_t *lista)
{
    return (lista->tamanio+1);
}

void lista_destruir(lista_t *lista, void destruir_dato(void *))
{
    nodo_t*nodo_aux;
    nodo_t*temp;
    nodo_aux=lista->primero;
    if(!lista_esta_vacia(lista))
    {
        while (nodo_aux)
        {

            temp=nodo_aux;
            nodo_aux=nodo_aux->siguiente;
            if(destruir_dato!=NULL)
            {
                destruir_dato(temp);
            }

        }
    }
    free(lista);
}

lista_iter_t *lista_iter_crear(const lista_t *lista)
{
    lista_iter_t* aux;
    aux=malloc(sizeof(lista_iter_t));
    aux->actual=lista->primero;
    aux->anterior=NULL;
    return aux;
}

bool lista_iter_avanzar(lista_iter_t *iter)
{
    nodo_t* aux;
    aux=lista_ver_siguiente(iter);
    if (iter->actual!=NULL)
    {
        iter->anterior=iter->actual;
        iter->actual=aux;
        return true;
    }
    else
    {
        return false;
    }
}

void *lista_iter_ver_actual(const lista_iter_t *iter)
{
    if (iter->actual!=NULL)
    {
        return iter->actual->data;
    }
    else
    {
        return NULL;
    }
}

bool lista_iter_al_final(const lista_iter_t *iter)
{
    if (iter->actual!=NULL)
    {
        return false;
    }
    else
    {
        return true;
    }
}

void lista_iter_destruir(lista_iter_t *iter)
{
    free(iter);
}

bool lista_insertar(lista_t *lista, lista_iter_t *iter, void *dato)
{
    nodo_t *nodo_aux;
    nodo_aux=nodo_crear();
    if (nodo_aux==NULL) return false;
    nodo_aux->data=dato;
    if (iter->actual==NULL) // La lista esta vacia
    {
        nodo_aux->siguiente=NULL;
        lista->primero=nodo_aux;
        lista->ultimo=nodo_aux;
        lista->tamanio++;
        iter->actual=lista->primero;
        return true;
    }
    else
    {
        if(iter->anterior==NULL) // Inserte al principio
        {
            iter->anterior=nodo_aux;
            iter->anterior->siguiente=iter->actual;
            lista->primero=iter->anterior;
            lista->ultimo=iter->actual;
            iter->actual=iter->anterior;
            iter->anterior=NULL;
            lista->tamanio++;
            return true;
        }
    }
    nodo_aux->siguiente=iter->actual;  // Inserte en el medio
    iter->anterior->siguiente=nodo_aux;
    iter->actual=nodo_aux;
    lista->tamanio++;
    return true;
}

void *lista_borrar(lista_t *lista, lista_iter_t *iter)
{
    nodo_t* aux;
    nodo_t* retorno;
    if (lista_esta_vacia(lista))
    {
        retorno=NULL;
    }
    else
    {
        if (iter->anterior==NULL) // Tiene un solo elemento
        {
            lista->primero=NULL;
            lista->ultimo=NULL;
            retorno=iter->actual;
            iter->actual=NULL;
        }
        else
        {
            iter->anterior->siguiente=iter->actual->siguiente;
            retorno=iter->actual;
            iter->actual=iter->actual->siguiente;
            if(iter->actual==NULL) //Borro el ultimo elemento de la lista
            {
                aux=lista->primero;
                iter->actual=iter->anterior;
                lista->ultimo=iter->actual;
                while (aux->siguiente!=lista->ultimo)
                {
                    aux=aux->siguiente;
                }
                iter->anterior=aux;
            }
        }
        lista->tamanio--;
    }
    return retorno;
}

void* lista_ver_siguiente(lista_iter_t *iter)
{
    if (iter->actual!=NULL)
    {
        return iter->actual->siguiente;
    }
    else
    {
        return NULL;
    }
}

