#include "lista.h"
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

/* ******************************************************************
 *                        PRUEBAS UNITARIAS
 * *****************************************************************/
void destruir_enteros(void* entero)
{
    free((int*)entero);
}

void destruir_strings(void* strings)
{
    free((char*)strings);
}
/* Función auxiliar para imprimir si estuvo OK o no. */
void print_test(char* name, bool result, int valor)
{
    printf("%s: %s %d\n", name, result? "OK" : "ERROR", valor);
}
/* Prueba que las primitivas de la cola funcionen correctamente. */


void prueba_enteros()
{
    int i;
    int j;
    int k;
    int* aux;
    lista_t *lista_1;
    lista_1 = lista_crear();
    i=5;
    j=10;
    k=15;
    print_test("Prueba lista_esta_vacia",lista_esta_vacia(lista_1),1);
    print_test("Prueba ver lista_primero",lista_ver_primero(lista_1)==NULL,1);
    print_test("Prueba que la lista inserte al principio el numero 5:",lista_insertar_primero(lista_1,&i),5);
    print_test("Prueba que la lista inserte al principio el numero 10:",lista_insertar_primero(lista_1,&j),10);
    print_test("Prueba lista_ver_primero", *(int*)lista_ver_primero(lista_1)==j,j);
    aux=(int*)lista_borrar_primero(lista_1);
    print_test("Prueba lista lista_borrar_primero",*aux==j,j);
    print_test("Prueba que la lista inserte al final el numero 15:",lista_insertar_ultimo(lista_1,&k),15);
    print_test("Prueba ver el largo de la lista",lista_largo(lista_1)==2,2);
    print_test("Prueba lista_esta_vacia",!lista_esta_vacia(lista_1),1);
    for (k=20; k<41; k++)
    {
        /*Prueba de volumen*/
        print_test("Prueba que la lista inserte al final el numero:",lista_insertar_ultimo(lista_1,&k),k);
    }
    print_test("Prueba ver el largo de la lista",lista_largo(lista_1)==23,23);
    print_test("Prueba ver lista_primero",*(int*)lista_ver_primero(lista_1)==i,i);
    lista_destruir(lista_1,destruir_enteros);



}
void prueba_lista()
{
    prueba_enteros();
}

void imprimir_super(lista_t *super)
{
    lista_iter_t *iter = lista_iter_crear(super);
    int i = 1;
    while (!lista_iter_al_final(iter))
    {
        char *elemento;
        /*void* aux;*/
        if (i==4){
            /*aux=lista_borrar(super,iter);
            free(aux);*/
        }else{
        if (i==3){
        elemento= lista_iter_ver_actual(iter);
        lista_insertar(super,iter,"carne");
        printf("%d. %s\n", i, (char *)elemento);
        i++;
        }
        elemento= lista_iter_ver_actual(iter);
        printf("%d. %s\n", i, (char *)elemento);}
        lista_iter_avanzar(iter);
        i++;
    }
    lista_iter_destruir(iter);
}

void imprimir_super2(lista_t *super)
{
    char* a="queso";
    char* b="pan";
    char* c="huevos";
    char* d="leche";
    char* e="papas";
    void* aux;
    bool verif=false;
    lista_iter_t *iter = lista_iter_crear(super);
    verif=false;
    verif=verif;
    /*printf("%s\n",(char*)lista_iter_ver_actual(iter));leche*/
    lista_insertar(super,iter,a);
    printf("%s\n",(char*)lista_iter_ver_actual(iter));/*queso*/
    verif=lista_iter_avanzar(iter);
    printf("%s\n",(char*)lista_iter_ver_actual(iter));/*queso*/
    verif=lista_iter_avanzar(iter);
    aux=lista_borrar(super,iter);
    free(aux);
    verif=lista_iter_avanzar(iter);
    lista_insertar(super,iter,b);
    printf("%s\n",(char*)lista_iter_ver_actual(iter));/*pan*/
    lista_iter_avanzar(iter);
    printf("%s\n",(char*)lista_iter_ver_actual(iter));
    lista_iter_avanzar(iter);
    printf("%s\n",(char*)lista_iter_ver_actual(iter));
    lista_iter_avanzar(iter);
    printf("%s\n",(char*)lista_iter_ver_actual(iter));
    lista_insertar(super,iter,c);
    printf("%s\n",(char*)lista_iter_ver_actual(iter));
    lista_insertar(super,iter,d);
    printf("%s\n",(char*)lista_iter_ver_actual(iter));
    lista_insertar(super,iter,e);
    printf("%s\n",(char*)lista_iter_ver_actual(iter));
    aux=lista_borrar(super,iter);
    free(aux);
    printf("%s\n",(char*)lista_iter_ver_actual(iter));
    aux=lista_borrar(super,iter);
    free(aux);
    printf("%s\n",(char*)lista_iter_ver_actual(iter));
    aux=lista_borrar(super,iter);
    free(aux);
    printf("%s\n",(char*)lista_iter_ver_actual(iter));
    aux=lista_borrar(super,iter);
    free(aux);
    printf("%s\n",(char*)lista_iter_ver_actual(iter));
    lista_iter_destruir(iter);
}

void prueba_iterador()
{
    lista_t *super;
    super = lista_crear();

    lista_insertar_ultimo(super, "leche");
    lista_insertar_ultimo(super, "huevos");
    lista_insertar_ultimo(super, "pan");
    /*lista_insertar_ultimo(super, "mermelada");*/

    imprimir_super2(super);

    lista_destruir(super, destruir_strings);
}

/* ******************************************************************
 *                        PROGRAMA PRINCIPAL
 * *****************************************************************/

int main()
{
    prueba_lista();
    prueba_iterador();
    return 0;
}


