#include "cola.h"
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

/* ******************************************************************
 *                        PRUEBAS UNITARIAS
 * *****************************************************************/
typedef struct alumnos
{
    int padron;
    char* nombre;
    char* apellido;
} alumnos_t;

typedef struct autos
{
    int patente;
    char* modelo;
    char* marca;
    char* dueno;
} autos_t;

void destruir_alumnos(void* alumnos)
{
    free((alumnos_t*)alumnos);
}

void destruir_autos(void* autos)
{
    free((autos_t*)autos);
}

void destruir_colas(void* colas)
{
    cola_destruir((cola_t*)colas,destruir_autos);
}

/* Funci�n auxiliar para imprimir si estuvo OK o no. */
void print_test(char* name, bool result, int valor)
{
    printf("%s: %s %d\n", name, result? "OK" : "ERROR", valor);
}
/* Prueba que las primitivas de la cola funcionen correctamente. */
void prueba_enteros(){
    int i;
    int j;
    int k;
    int* aux;
    int* aux2;
    int* aux3;
    cola_t *cola_1;
    cola_1 = cola_crear();
    i=5;
    j=10;
    k=15;
    print_test("Prueba cola_esta_vacia",cola_esta_vacia(cola_1),1);
    print_test("Prueba ver cola_primero",cola_ver_primero(cola_1)==NULL,1);
    print_test("Prueba que la cola encole el numero 5",cola_encolar(cola_1,&i),5);
    print_test("Prueba que la cola encole el numero 10",cola_encolar(cola_1,&j),10);
    print_test("Prueba cola_ver_primero", *(int*)cola_ver_primero(cola_1)==i,1);
    aux=(int*)cola_desencolar(cola_1);
    print_test("Prueba cola_desencolar",*aux==i,1);
    print_test("Prueba que la cola encole el numero 15",cola_encolar(cola_1,&k),15);
    aux2=(int*)cola_desencolar(cola_1);
    print_test("Prueba cola_desencolar",*aux2==j,1);
    aux3=(int*)cola_desencolar(cola_1);
    print_test("Prueba cola_desencolar",*aux3==k,1);
    print_test("Prueba cola_esta_vacia", cola_esta_vacia(cola_1),1);
    print_test("Prueba cola_desencolar", cola_desencolar(cola_1)==NULL,1);
    print_test("Prueba cola_ver_primero",cola_ver_primero(cola_1)==NULL,1);
    print_test("Prueba cola_esta_vacia",cola_esta_vacia(cola_1),1);
    cola_destruir(cola_1,NULL);
}

void prueba_alumnos(){
    alumnos_t* alu1;
    alumnos_t* alu2;
    alumnos_t* alu3;
    cola_t *cola_1;
    alu1=malloc(sizeof(alumnos_t));
    alu2=malloc(sizeof(alumnos_t));
    alu3=malloc(sizeof(alumnos_t));
    alu1->padron=9191;
    alu1->nombre="Carlos";
    alu1->apellido="Perez";
    alu2->padron=1394;
    alu2->nombre="Juan";
    alu2->apellido="Lopez";
    alu3->padron=2834;
    alu3->nombre="Miguel";
    alu3->apellido="Chavez";
    cola_1 = cola_crear(); /*Crea una cola*/
    print_test("Prueba cola_esta_vacia",cola_esta_vacia(cola_1),1); /*Prueba si la cola esta vacia.*/
    print_test("Prueba ver cola_primero",cola_ver_primero(cola_1)==NULL,1);
    print_test("Prueba que la cola encole el alumno 1",cola_encolar(cola_1,alu1),alu1->padron);
    print_test("Prueba que la cola encole el alumno 2",cola_encolar(cola_1,alu2),alu2->padron);
    print_test("Prueba cola_ver_primero",((alumnos_t*)cola_ver_primero(cola_1))->padron==alu1->padron,alu1->padron);
    print_test("Prueba cola_desencolar",((alumnos_t*)cola_desencolar(cola_1))->padron==alu1->padron,alu1->padron);
    print_test("Prueba que la cola encole el alumno 3",cola_encolar(cola_1,alu3),alu3->padron);
    print_test("Prueba cola_desencolar",((alumnos_t*)cola_desencolar(cola_1))->padron==alu2->padron,alu2->padron);
    free(alu1);
    free(alu2);
    cola_destruir(cola_1,destruir_alumnos);
}

void prueba_autos(){
    autos_t* auto1;
    autos_t* auto2;
    autos_t* auto3;
    autos_t* auto4;
    cola_t *cola_1;
    cola_t *cola_2;
    auto1=malloc(sizeof(autos_t));
    auto2=malloc(sizeof(autos_t));
    auto3=malloc(sizeof(autos_t));
    auto4=malloc(sizeof(autos_t));
    auto1->patente=238742;
    auto1->modelo="Gol";
    auto1->marca="Volkswagen";
    auto1->dueno="Juan Perez";
    auto2->patente=827492;
    auto2->modelo="Megane";
    auto2->marca="Renault";
    auto2->dueno="Esteban Maidana";
    auto3->patente=743781;
    auto3->modelo="Palio";
    auto3->marca="Fiat";
    auto3->dueno="Sergio Niclis";
    auto4->patente=127176;
    auto4->modelo="307";
    auto4->marca="Peugeot";
    auto4->dueno="Carlos Fernandez";
    cola_2 = cola_crear();
    cola_1 = cola_crear();
    print_test("Prueba que la cola encole el auto 1",cola_encolar(cola_1,auto1),auto1->patente);
    print_test("Prueba que la cola encole el auto 2",cola_encolar(cola_1,auto2),auto2->patente);
    print_test("Prueba que la cola encole el auto 3",cola_encolar(cola_1,auto3),auto3->patente);
    print_test("Prueba que la cola encole el auto 4",cola_encolar(cola_1,auto4),auto4->patente);
    print_test("Prueba que la cola 2 encole la cola 1",cola_encolar(cola_2,cola_1),1);
    cola_destruir(cola_2,destruir_colas);
}

void prueba_cola()
{
    prueba_enteros();
    prueba_alumnos();
    prueba_autos();
}

/* ******************************************************************
 *                        PROGRAMA PRINCIPAL
 * *****************************************************************/
int main(void)
{
    prueba_cola();
    return 0;
}
